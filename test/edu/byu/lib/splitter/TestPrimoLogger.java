/**
 * 
 */
package edu.byu.lib.splitter;

import com.exlibris.primo.api.common.IPrimoLogger;

/**
 * An implementation of {@link IPrimoLogger} for testing purposes.
 * 
 * @author Charles Draper
 */
public class TestPrimoLogger implements IPrimoLogger {

	@Override
	public void error(String arg0) {
		System.err.println("ERROR: " + arg0);
	}

	@Override
	public void error(String arg0, Exception arg1) {
		System.err.println("ERROR: " + arg0);
		arg1.printStackTrace();
	}

	@Override
	public void info(String arg0) {
		System.out.println("INFO: " + arg0);
	}

	@Override
	public void setClass(Class<?> arg0) {
		// no idea what this does
	}

	@Override
	public void warn(String arg0) {
		System.err.println("WARN: " + arg0);
	}

	@Override
	public void warn(String arg0, Exception arg1) {
		System.err.println("WARN: " + arg0);
		arg1.printStackTrace();
	}

}
