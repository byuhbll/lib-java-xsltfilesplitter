/**
 * 
 */
package edu.byu.lib.splitter;

import java.util.HashMap;
import java.util.Map;

import com.exlibris.primo.api.common.IPrimoLogger;
import com.exlibris.primo.api.spliter.plugin.IRecordSaver;

/**
 * Used for testing the {@link XsltFileSplitter} outside of Primo. Be sure to
 * modify the XSL path to a test XSL file.
 * 
 * @author Charles Draper
 */
public class TestMain {

	private XsltFileSplitter splitter;

	/**
	 * 
	 */
	public TestMain() {
		this.splitter = new XsltFileSplitter();
	}

	/**
	 * @throws Exception
	 */
	public void init() throws Exception {
		String charSet = "UTF-8";
		IPrimoLogger logger = new TestPrimoLogger();
		Map<String,Object> params = new HashMap<String,Object>();
		params.put(XsltFileSplitter.XSL_PATH_PARAM, "test/edu/byu/lib/splitter/test.xsl");
		splitter.init(charSet, logger, params);
	}

	/**
	 * @throws Exception
	 */
	public void parse() throws Exception {
		IRecordSaver saver = new TestRecordSaver();
		splitter.parse(null, null, saver);
	}

	/**
	 * @throws Exception
	 */
	public void doneParsing() throws Exception {
		splitter.doneParsing();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		TestMain test = new TestMain();

		test.init();
		test.parse();
		test.doneParsing();
	}

}
