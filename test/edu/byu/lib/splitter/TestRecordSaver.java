/**
 * 
 */
package edu.byu.lib.splitter;

import com.exlibris.primo.api.spliter.plugin.IRecordSaver;
import com.exlibris.primo.api.spliter.plugin.RecordData;

/**
 * An implementation of {@link IRecordSaver} for testing purposes. It does not
 * actually save anything. Instead it prints to the console whether or not it's
 * deleting or saving a record.
 * 
 * @author Charles Draper
 */
public class TestRecordSaver implements IRecordSaver {

	@Override
	public void save(RecordData arg0) throws Exception {
		String sourceId = arg0.getIdentifier();
		String xml = arg0.getRecordData();
		boolean deleted = arg0.isDeleted();
		
		if(deleted) {
			System.out.println("Deleting: " + sourceId);
		} else {
			String xmlOneLine = xml.replaceAll(">\\s+<", "><").replaceAll("\\s+", " ");
			String xmlShort = xmlOneLine.substring(0, Math.min(xmlOneLine.length(), 256));
			System.out.println("Saving: " + sourceId + " " + xmlShort);
		}
	}

}
