<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
	version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" />

<xsl:template match="*">
	<xsl:copy>
		<xsl:attribute name="transformed">true</xsl:attribute>
		<xsl:copy-of select="*" />
	</xsl:copy>
</xsl:template>

</xsl:stylesheet>
