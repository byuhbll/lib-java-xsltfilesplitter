/**
 * 
 */
package edu.byu.lib.splitter;

import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.exlibris.primo.api.common.IPrimoLogger;
import com.exlibris.primo.api.spliter.plugin.IRecordSaver;
import com.exlibris.primo.api.spliter.plugin.RecordData;
import com.exlibris.primo.publish.platform.harvest.splitters.generic.DomXmlSplitter;

/**
 * {@link XsltFileSplitter} is a sub class of {@link DomXmlSplitter} whose
 * function is to run an XSL transformation on each XML document that is split
 * by the {@link DomXmlSplitter} prior to being saved to Primo.
 * 
 * @author Charles Draper
 */
public class XsltFileSplitter extends DomXmlSplitter {

	/**
	 * Primo Back Office parameter name specifying the location of the XSLT file.
	 * Required.
	 */
	public static final String XSL_PATH_PARAM = "XslPath";

	/**
	 * Primo Back Office parameter name specifying which implementation of
	 * {@link TransformerFactory} to use. Optional. Default =
	 * net.sf.saxon.TransformerFactoryImpl unless not found, then default is the
	 * {@link TransformerFactory} for the JVM.
	 */
	public static final String TRANSFORMER_FACTORY_CLASS_PARAM = "TransformerFactoryClass";
	
	/**
	 * Primo Back Office parameter name to tell the splitter to print out extra
	 * debug information including the XML before and after the transformation.
	 * Optional. Default = false.
	 */
	public static final String DEBUG_PARAM = "Debug";

	/**
	 * Saxon's {@link TransformerFactory} class name.
	 */
	public static final String PREFERRED_TRANSFORMER_FACTORY_CLASS = "net.sf.saxon.TransformerFactoryImpl";

	/**
	 * Print out number of processed records at this interval.
	 */
	public static final int LOG_INTERVAL = 100;

	/**
	 * Primo logging mechanism
	 */
	private IPrimoLogger logger;
	
	/**
	 * The XSLT transformer
	 */
	private Transformer transformer;

	/**
	 * Whether or not to print out extra debug information including the XML
	 * before and after the transformation.
	 */
	private boolean debug;
	
	/**
	 * Running count of processed records.
	 */
	private long processed;

	/**
	 * Running count of failed transformations.
	 */
	private long errors;

	/**
	 * Initializes the file splitter.
	 */
	@Override
	public void init(String charSet, IPrimoLogger logger, Map<String,Object> params) throws Exception {
		// get the Primo logger
		this.logger = logger;
		
		// get XSL Path from Back Office parameters
		String xslPath = (String)params.get(XSL_PATH_PARAM);
		logger.setClass(XsltFileSplitter.class);
		logger.info(XSL_PATH_PARAM + "=" + xslPath);

		// get TransformerFactory class from Back Office parameters
		String transformerFactoryClass = (String)params.get(TRANSFORMER_FACTORY_CLASS_PARAM);

		// if not specified, try Saxon first
		if(transformerFactoryClass == null) {
			transformerFactoryClass = PREFERRED_TRANSFORMER_FACTORY_CLASS;
		}

		logger.info(TRANSFORMER_FACTORY_CLASS_PARAM + "=" + transformerFactoryClass + " (configured)");

		TransformerFactory factory = null;

		try {
			// try instantiating the given TransformerFactory or preferred if none given
			factory = TransformerFactory.newInstance(transformerFactoryClass, this.getClass().getClassLoader());
		} catch(TransformerFactoryConfigurationError e) {
			// if a TransformerFactory was specified in configuration, but failed to
			// instantiate, fail here
			if(params.containsKey(TRANSFORMER_FACTORY_CLASS_PARAM)) {
				logger.error("Unable to find TransformerFactory " + transformerFactoryClass + " : " + e);
				throw e;
			} else {
				// if no TransformerFactory was specified in configuration and preferred
				// failed to instantiate, try the JVM's default
				logger.error("Unable to find TransformerFactory " + transformerFactoryClass + ", using JVM default TransformerFactory");
				factory = TransformerFactory.newInstance();
			}
		}

		logger.info(TRANSFORMER_FACTORY_CLASS_PARAM + "=" + factory.getClass().getCanonicalName() + " (actual)");

		// compile the XSL
		Source xslSource = new StreamSource(new File(xslPath));
		Templates templates = factory.newTemplates(xslSource);
		
		// create a transformer from the compiled XSL that will be used to transform
		// all documents
		this.transformer = templates.newTransformer();
		
		logger.info("Set the following parameters to the XSL:");
		// set ALL File Splitter params to the transformer, so that they will be
		// available in the XSL as well
		for(String name : params.keySet()) {
			logger.info(name + "=" + params.get(name));
			transformer.setParameter(name, params.get(name));
		}
		
		// get debug from Back office parameters
		String debug = (String)params.get(DEBUG_PARAM);
		logger.info(DEBUG_PARAM + "=" + debug + " (configured)");

		// convert the string representation to a boolean
		if(debug != null) {
			this.debug = Boolean.parseBoolean(debug);
		}

		logger.info(DEBUG_PARAM + "=" + this.debug + " (actual)");
		
		// initialize the DomXmlSplitter
		super.init(charSet, logger, params);
	}

	/**
	 * Swaps out the Primo supplied {@link IRecordSaver} for our own that will
	 * perform the transformation. Other than that, just call the DomXmlSplitter's
	 * parse method.
	 */
	@Override
	public void parse(InputStream input, File file, IRecordSaver saver) throws Exception {
		// wrap the Primo given record saver with our own that performs the transformation 
		XslRecordSaver xslSaver = new XslRecordSaver(saver);
		
		// call parse on the DomXmlSplitter
		super.parse(input, file, xslSaver);
	}
	
	/**
	 * Making sure things get garbage collected.
	 */
	@Override
	public void doneParsing() {
		logger.info((processed + errors) + " processed: " + processed + " successful, " + errors + " errors.");
		transformer = null;
		logger = null;
		super.doneParsing();
	}

	/**
	 * Wraps the {@link IRecordSaver} that is supplied by Primo in order to
	 * perform an XSL transformation on the split XML prior to actually saving it
	 * to Primo.
	 * 
	 * @author Charles Draper
	 */
	public class XslRecordSaver implements IRecordSaver {

		/**
		 * The original Primo supplied saver. It will be used to save the final
		 * transformed XML.
		 */
		private IRecordSaver saver;

		/**
		 * Creates a new {@link XslRecordSaver}
		 * 
		 * @param saver the Primo supplied {@link IRecordSaver}
		 */
		public XslRecordSaver(IRecordSaver saver) {
			this.saver = saver;
		}

		/**
		 * Transforms the split XML using the configured {@link Transformer} and
		 * saves the result to Primo.
		 */
		@Override
		public void save(RecordData record) throws Exception {

			// only transform the record if it is not deleted
			if(record.isDeleted()) {
				processed++;
			} else {
				try {
					// get the XML that was split out using the DomXmlSplitter
					String xmlData = record.getRecordData();

					// create the {@link Reader} for the XML
					StringReader reader = new StringReader(xmlData);

					// create the object to store the transformed result
					StringWriter writer = new StringWriter();

					if(debug) {
						logger.info("Source XML for " + record.getIdentifier());
						logger.info(xmlData);
					}

					// Run the XSLT transformation
					transformer.transform(new StreamSource(reader), new StreamResult(writer));

					// get the resulting XML String
					String transformedData = writer.toString();

					if(debug) {
						logger.info("Transformed XML for " + record.getIdentifier());
						logger.info(transformedData);
					}

					// set the resulting XML to the record
					record.setRecordData(transformedData);

					processed++;

				} catch(Exception e) {
					logger.error("Record " + record.getIdentifier() + " failed to process : " + e);
					errors++;

					// if transformation fails, set record data to null so Primo will fail
					// the record
					record.setRecordData(null);
				}
			}

			// save the record
			saver.save(record);

			// print out progress every so many records
			if((processed + errors) % LOG_INTERVAL == 0) {
				logger.info("Processed " + processed + " records successfully.");
			}
			
		}
	}
}
