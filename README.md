Overview
========

The XSLT File Splitter is a Primo plugin that performs XSLT transformations on source XML documents prior to submitting them to Primo. The XSLT File Splitter implements Primo’s File Splitter framework and extends the built-in XML File Splitter in order to accomplish this.

A File Splitter is a Primo plugin which facilitates the harvesting of your source data into Primo. For example, if you have multiple records in one file, the records are in an unrecognized format, or the records are accessible in an unsupported way, a File Splitter can help get those records to a format that Primo will understand. It becomes a translator for Primo; translating your various repositories and structures into individual records that Primo can ingest.

There are several built in File Splitters to help with harvesting HTML or XML files or other formats. Because it’s a plugin, you can write your own, if you're familiar with the Java programming language, when your data are in an unsupported structure.

The XSLT File Splitter, written by Brigham Young University, is one such plugin. It extends the built-in Primo XML File Splitter and works in very much the same way with the exception that it performs an XSLT transformation just prior to saving the record to Primo. So the XSLT File Splitter takes an XML file, splits it up into individual records in the case of collections of records, and performs an XSLT transformation on each record prior to handing it off to Primo for further processing such as NEP, Dedup, and FRBR.

Why XSLT?
=========
XSLT is a language used to transform a source XML document into something else. In this case that something else is another XML document of different structure. XSLT is very powerful giving the creator of an XSLT stylesheet many familiar programming features.

The three main reasons we use XSLT:

1. Having multiple institutions with mostly the same, but slightly different normalization rules meant that we had large copies of essentially the same rules over and over again. Any time we had to make a change, we had to modify all of these copies. XSLT allowed us to reuse the same code for each institution and interject the variances using if-else statements.

2. Some normalization rules were extremely lengthy and very complex. Because of the power of XSLT, we were able to greatly simplify the logic.

3. XSLT is a programming language and therefore gave us added flexibility when creating the PNX.

The Primo documentation refers to some issues with using an XSL transformer during Primo harvesting. The XSL File Splitter overcomes most of these issues.

1. Performance – Transformation performance is greatly enhanced by using Saxon the XSLT processor. Saxon, created by Michael Kay, is a very powerful and fast XSLT processor.

2. Memory consumption – The transformation takes place after the file has been split, not before. Therefore, much less memory is required because each transformation is done on an individual record instead of an entire collection.

3. Complexity – XSLT is a programming language so there's no getting around having to learn and become familiar with it. XSLT stylesheets require some technical background; however, we’ve found them easier to maintain than the normalization rules.

4. Flexibility – Using a more advanced XSLT processor like Saxon allows one to use XSLT 2.0 (free) or even XSLT 3.0 (paid). These technologies give an enormous amount of flexibility. Saxon also makes it possible to write plugins for XSLT using Java.


Getting Started
===============

Let's get started with the XSLT File Splitter. Here are some requirements before beginning.

* Access to and knowledge of the Primo Back Office
* Source files must be in XML format

1. Add file splitter to mapping tables
--------------------------------------

Log into the Primo Back Office

Advanced configuration -> Mapping tables -> File Splitters
	Create new file splitter "XSL file splitter | edu.byu.lib.splitter.XsltFileSplitter | XSLT file splitter"


2. Configure the file splitter
------------------------------

Advanced Configured -> All Mapping Tables -> File Splitters Params

Here you will configure both the XML File Splitter and the XSLT File Splitter. Please refer to Primo’s documentation to configure the XML File Splitter (StatusXpath, FullRecordXPath, StatusWhenDeleted, RootXpath, IdentifierXpath). 

XSLT File Splitter Parameters:

XslPath = specifies the location of the XSLT file to use for all transformations. (Required)

TransformerFactoryClass = specifies which implementation of TransformerFactory to use. Default is net.sf.saxon.TransformerFactoryImpl (Saxon); JVM default if not found. (Optional)

Debug = true|false to tell the splitter to print out extra debug information including the XML before and after the transformation. Default = false. (Optional)

All of theses parameters and any additional ones you add will be passed to the XSL using <xsl:param name="..." />. Note that all parameter names are lower-cased by the File Splitter framework so they will be lower-cased going into the XSL.

3. Add new Data Source
----------------------

Ongoing configurations -> Pipe config -> Data source

4. Create 1:1_XML Normalization mapping set
-------------------------------------------

If you wish to completely use XSLT to create the PNX, then set up a 1:1 mapping set like this.

Try using pentadactly marco function to avoid having to create each rule manually. Use it to record one rule with tabs and  then play it to create all rules in the category.

Required PNX fields

Loading records or using transforms that do not include these fields will result in an error in the pipe error log

* control/sourceid
* control/sourcerecordid
* control/recordid
* display/title
* display/type
* display/title
* display/availlibrary
* links/ - “required” but don’t seem to cause errors if left out
* search/sourceid
* ranking/booster1

5. Create Pipe
--------------

Primo monitoring -> Pipe Monitoring -> Create pipe

Source directory = location of your xml files to harvest. 

6. Download the XSLT File Splitter
----------------------------------

Please download the XSLT File Splitter from here:

https://bitbucket.org/byuhbll/lib-java-xsltfilesplitter/src

You will need to copy or symlink the xslt-file-splitter.jar into Primo’s file splitter plugin location

$ cp xslt-file-splitter.jar /exlibris/primo/p4_1/ng/primo/home/profile/publish/publish/production/conf/fileSplitter/lib

You can use the supplied XSL files in the xsl folder or create your own. Note that the supplied XSL files are for marc xml and will not be correct nor complete for your institution. These files will need to be accessible by the file splitter so place them somewhere on your Primo server and configure the splitter with the XslPath parameter.

Please refer to Primo’s documentation on how to make your XML files accessible to the XML File Splitter.

7 - Download Saxon
------------------

Download Saxon-HE (saxon9he.jar) from http://saxon.sourceforge.net/ and place it in the same directory as the file splitter.

8 - Run your pipe
-----------------

References
==========

* http://saxon.sourceforge.net/
* http://www.saxonica.com/
* http://www.saxonica.com/documentation/html/xsl-elements/
* http://www.w3schools.com/xsl/
* http://zvon.org/xxl/XPathTutorial/General/examples.html


