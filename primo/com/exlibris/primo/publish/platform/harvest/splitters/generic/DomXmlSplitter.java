/**
 * 
 */
package com.exlibris.primo.publish.platform.harvest.splitters.generic;

import java.io.File;
import java.io.InputStream;
import java.util.Map;

import com.exlibris.primo.api.common.IPrimoLogger;
import com.exlibris.primo.api.spliter.plugin.IFileSplitter;
import com.exlibris.primo.api.spliter.plugin.IRecordSaver;
import com.exlibris.primo.api.spliter.plugin.RecordData;

/**
 * In order to compile the {@link XsltFileSplitter}, the {@link DomXmlSplitter}
 * needs to be on the classpath. Instead of finding the real
 * {@link DomXmlSplitter}, we simply create a dummy one here. This class should
 * NOT be included in the final jar.
 * 
 * @author Charles Draper
 */
public class DomXmlSplitter implements IFileSplitter {
	
	private IPrimoLogger logger;
	
	@Override
	public void init(String charSet, IPrimoLogger logger, Map<String,Object> params) throws Exception {
		this.logger = logger;
		logger.info("super.init()");
	}

	@Override
	public void parse(InputStream input, File file, IRecordSaver saver) throws Exception {
		logger.info("super.parse()");
		
		// for testing purposes
		for(int i = 0; i < 2; i++) {
			RecordData record = new RecordData(i + "", "<record><id>" + i + "</id><title>Title " + i + "</title></record>", false);
			saver.save(record);
		}
		
		for(int i = 2; i < 4; i++) {
			RecordData record = new RecordData(i + "", "<record />", true);
			saver.save(record);
		}
	}
	
	@Override
	public void doneParsing() {
		logger.info("super.doneParsing()");
	}

}
