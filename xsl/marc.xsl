<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	version="2.0" 
	xmlns:lib="http://lib.byu.edu/"
	xmlns:map="urn:schemas-microsoft-com:office:spreadsheet"
	exclude-result-prefixes="lib map xd"
	xmlns:saxon="http://icl.com/saxon"
	extension-element-prefixes="saxon">


<xsl:import href="util.xsl"/>
<xsl:import href="marcAddata.xsl"/>
<xsl:import href="marcControl.xsl"/>
<xsl:import href="marcDedup.xsl"/>
<xsl:import href="marcDelivery.xsl"/>
<xsl:import href="marcDisplay.xsl"/>
<xsl:import href="marcEnrichment.xsl"/>
<xsl:import href="marcFacets.xsl"/>
<xsl:import href="marcFrbr.xsl"/>
<xsl:import href="marcLinks.xsl"/>
<xsl:import href="marcRanking.xsl"/>
<xsl:import href="marcSearch.xsl"/>
<xsl:import href="marcSort.xsl"/>

<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" 
	saxon:character-representation="native;decimal" />

<xsl:variable name="institution" select="'EDU'" />
<xsl:variable name="source" select="'edu'" />

<xd:doc>Mapping table: Determines availability based on home location or current location + library code</xd:doc>
<xsl:variable name="availability" select="document('map/availability.xml')" />

<xd:doc>Mapping table: Uses item type to determine facets, display formats, image link etc. 
		Item type is translated to resource type facets, prefilter facets and the code for the search engine </xd:doc>
<xsl:variable name="itemTypes" select="document('map/itemTypes.xml')" />
	
<xd:doc>Mapping table: Supports Research Information Systems data types for citation exchange.
		http://en.wikipedia.org/wiki/RIS_(file_format)</xd:doc>
<xsl:variable name="risType" select="document('map/ristype.xml')" />

<xd:doc>Mapping table: Institutional Library codes to Primo codes. 
		Maps various library codes to the respective primo codes like SFX and ON-DEMAND to BYUINTERNET</xd:doc>
<xsl:variable name="symphonyLibraryCodes" select="document('map/symphonyLibraryCodes.xml')" />

<xd:doc>Mapping table: Gives description based on location code. 
		For instance HONORS gets Honors Reading Room, Harold B. Lee Library</xd:doc>
<xsl:variable name="symphonyLocations" select="document('map/symphonyLocations.xml')" />

<xd:doc>Mapping table: Gives description of note field based on MARC field number. 
		For instance 565 get "file size"</xd:doc>
<xsl:variable name="symphonyNotes" select="document('map/symphonyNotes.xml')" />

<xd:doc>Mapping table: for Symphony item types. 
		Translates the Symphony items types (999|t) to an item type</xd:doc>
<xsl:variable name="symphonyTypes" select="document('map/symphonyTypes.xml')" />


<xsl:template match="/">
	<xsl:apply-templates select="//record" />
</xsl:template>


<!-- creates the record -->
<xd:doc>Creates the record. This calls all the other templates for creating a full PNX record </xd:doc>
<xsl:template match="record">

	<!-- Gather data -->
	<xsl:variable name="record">
		<record>
			<xsl:call-template name="control" />
			<xsl:call-template name="display" />
			<xsl:call-template name="links" />
			<xsl:call-template name="search" />
			<xsl:call-template name="sort" />
			<xsl:call-template name="facets" />
			<xsl:call-template name="dedup" />
			<xsl:call-template name="frbr" />
			<xsl:call-template name="delivery" />
			<xsl:call-template name="enrichment" />
			<xsl:call-template name="ranking" />
			<xsl:call-template name="addata" />
		</record>
	</xsl:variable>
	
	<!-- Clean up data -->
	<xsl:copy-of select="lib:normalize-pnx($record)" />
	
</xsl:template>


</xsl:stylesheet>
