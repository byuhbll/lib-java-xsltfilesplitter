<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:lib="http://lib.byu.edu/"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	exclude-result-prefixes="lib xd"
	xmlns:saxon="http://icl.com/saxon"
	extension-element-prefixes="saxon">
	


<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" 
	saxon:character-representation="native;decimal" />

<!-- creates the sort section -->
<xd:doc>Sort is loaded into the Jaguar - Lucene search engine and provides sorting functions for listed fields </xd:doc>
<xsl:template name="sort">
	<sort>
		<xsl:call-template name="sort-title" />
		<xsl:call-template name="sort-creationdate" />
		<xsl:call-template name="sort-author" />
	</sort>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the sort-title section -->
<xd:doc>Provides sort by title from 245|ab and replaces punctuation ,/=.\^ with ,</xd:doc>
<xsl:template name="sort-title">

	<title><xsl:value-of select="lib:merge(lib:fields-replace(datafield[@tag='245']/subfield[matches(@code,'[ab]')],'[,/=.\^]$',''))" /></title>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the sort-creationdate section -->
<xd:doc>Provides sort by date from lib:dates method</xd:doc>
<xsl:template name="sort-creationdate">
	<xsl:variable name="date">
		<xsl:call-template name="display-creationdate"/>
	</xsl:variable>
	
	<xsl:if test="$date != ''">
			<creationdate><xsl:value-of select="$date" /><xsl:text>0101</xsl:text></creationdate>
	</xsl:if>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the sort-author section -->
<xd:doc>Provides sort by author from 100|a replacing punctuation with ''</xd:doc>
<xsl:template name="sort-author">

	<author><xsl:value-of select="replace(datafield[@tag='100'][1]/subfield[@code='a'][1],concat('[,;&quot;=/\\',$apos,']$'),'')" /></author>
	
</xsl:template>


</xsl:stylesheet>
