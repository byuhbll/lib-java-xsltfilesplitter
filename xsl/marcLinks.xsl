<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:lib="http://lib.byu.edu/"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	exclude-result-prefixes="lib xd"
	xmlns:saxon="http://icl.com/saxon"
	extension-element-prefixes="saxon">
	


<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" 
	saxon:character-representation="native;decimal" />

<!-- creates the links section -->
<xd:doc>Links are created to provide weblinks to varying resources connected to a record via templates. Templates provide a layer of generalization, providing headers in links for particular record sets.</xd:doc>
<xsl:template name="links">
	<links>
		<xsl:call-template name="links-openurl" />
		<xsl:call-template name="links-backlink" />
		<xsl:call-template name="links-linktorsrc" />
		<xsl:call-template name="links-thumbnail" />
		<xsl:call-template name="links-linktotoc" />
		<xsl:call-template name="links-linktoabstract" />
		<xsl:call-template name="links-openurlfulltext" />
		<xsl:call-template name="links-linktoholdings" />
		<xsl:call-template name="links-linktoreview" />
		<xsl:call-template name="links-addlink" />
		<xsl:call-template name="links-linktouc" />
		<xsl:call-template name="links-linktofa" />
		<xsl:call-template name="links-linktoexcerpt" />
	</links>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the links/openurl section -->
<xsl:template name="links-openurl">
	<xsl:variable name="type">
		<xsl:call-template name="display-type" />
	</xsl:variable>
	<openurl>
		<xsl:choose>
			<xsl:when test="$type='article'">
				$$Topenurl_article
			</xsl:when>
			<xsl:otherwise>
				$$Topenurl_journal
			</xsl:otherwise>
		</xsl:choose>	
	</openurl>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the links/backlink section -->
<xsl:template name="links-backlink">
	<backlink>$$Tunicorn_backlink$$DLegacy catalog</backlink>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the links/linktorsrc section 
	Inhaltsverzeichnis means Table of Contents in German
-->
<xsl:template name="links-linktorsrc">

	<xsl:if test="datafield[@tag='856']/subfield[@code='u' and matches(.,'http://byugle.lib.byu.edu/player')]">
		<xsl:for-each select="datafield[@tag='856']/subfield[@code='u']">
			<linktorsrc>$$U<xsl:value-of select="." />$$DLink to Resource</linktorsrc>
		</xsl:for-each>
	</xsl:if>

	<xsl:if test="datafield[@tag='538']/subfield[@code='a' and matches(lower-case(.),'byugle')]">
		<xsl:for-each select="datafield[@tag='856']/subfield[@code='u']">
			<linktorsrc>$$U<xsl:value-of select="." />$$DLink to Resource</linktorsrc>
		</xsl:for-each>
	</xsl:if>
	
	<!-- We used to filter by ind=1 to determine link type - due to cataloging problems we now manually check for http in the link string -->
	<!-- Ignore items with ind2 as they are related works -->
	<!-- filter out non-full text types indicated by 856|3 until we are displaying something other than "available online" for everything -->
	
	<xsl:for-each select="datafield[@tag='856' 
								and contains(.,'http') 
								and @ind2!='2'
								and not(subfield[@code='3' and matches(.,
								'Sample text|Excerpt|Sample|Inhaltsverzeichnis|table of contents
								|Table of contents only|Publisher description|Table of contents
								|Table of contents only|book review|Sample text|sample text|publisher description')])]">
			<linktorsrc>$$U<xsl:value-of select="subfield[@code='u']" />$$D<xsl:value-of select="lib:merge(subfield[matches(@code,'[yz3]')])" /></linktorsrc>
	</xsl:for-each>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the links/thumbnail section -->
<xsl:template name="links-thumbnail">
	<xsl:if test="datafield[@tag='020' and subfield[@code='a']]">
		<thumbnail>$$Tsyndetics_thumb</thumbnail>
	</xsl:if>
	<xsl:if test="datafield[matches(@tag, '020|010')]/subfield[@code='a']
				| datafield[@tag='035']/subfield[matches(@code, 'OCoLC')] ">
		<thumbnail>$$Tgoogle_thumb</thumbnail>
	</xsl:if>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the links/linktotoc section -->
<xsl:template name="links-linktotoc">
	<xsl:for-each select="datafield[@tag='505']/subfield[@code='u']">
		<linktotoc>$$U<xsl:value-of select="." />$$DTable of Contents</linktotoc>
	</xsl:for-each>
	<xsl:for-each select="datafield[@tag='856' and @ind1='4' and subfield[@code='3' and matches(lower-case(.),'table of contents|Table of contents
	|Table of contents only')]]">
		<linktotoc>$$U<xsl:value-of select="subfield[@code='u']" />$$D<xsl:value-of select="lib:merge(subfield[matches(@code,'[yz3]')])" /></linktotoc>		
	</xsl:for-each>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the links/linktoabstract section -->
<xsl:template name="links-linktoabstract">
	<xsl:if test="datafield[@tag='020' and subfield[@code='a']]">
		<linktoabstract>$$Tsyndetics_abstract</linktoabstract>
	</xsl:if>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the links/openurlfulltext section -->
<xsl:template name="links-openurlfulltext">
	<xsl:variable name="type">
		<xsl:call-template name="display-type" />
	</xsl:variable>
	<openurlfulltext>
		<xsl:choose>
			<xsl:when test="$type='article'">
				$$Topenurlfull_article
			</xsl:when>
			<xsl:otherwise>
				$$Topenurlfull_journal
			</xsl:otherwise>
		</xsl:choose>	
	</openurlfulltext>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the links/linktoholdings section -->
<xsl:template name="links-linktoholdings">
	
	<xsl:for-each select="datafield[@tag='856' and not(subfield[@code='3' and matches(.,
	
			'Table of contents|Publisher description|Table of contents only|Text version:|PDF version:|
			 Contributor biographical information|Online version|Full text available|Sample text|Text version|
			 Online version.|PDF version|	SpringerLink|	IEEE Xplore|	Google|	HathiTrust Digital Library|
			 table of contents|book review|sample text|publisher description')]) and subfield[@code='3']]">

	<linktoholdings>$$Tunicorn_holdings$$D<xsl:value-of select="subfield[@code='3']" /></linktoholdings>
	</xsl:for-each>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the links/linktoreview section -->
<xsl:template name="links-linktoreview">
	<xsl:if test="datafield[@tag='020' and subfield[@code='a']]">
		<linktoreview>$$Tsyndetics_review$$DCLink to review</linktoreview>
	</xsl:if>

	<xsl:for-each select="datafield[@tag='520']/subfield[@code='u']">
		<linktoreview>$$U<xsl:value-of select="." />$$DLink to review</linktoreview>
	</xsl:for-each>

	<xsl:for-each select="datafield[@tag='856' and subfield[@code='u'] and @ind1='4' and subfield[@code='3' and matches(lower-case(.),'book review')]]">
			<linktoreview>$$U<xsl:value-of select="subfield[@code='u']" />$$D<xsl:value-of select="lib:merge(subfield[matches(lower-case(@code),'[yz3]')])" /></linktoreview>		
	</xsl:for-each>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the links/addlink section -->
<xsl:template name="links-addlink">

	<xsl:for-each select="datafield[matches(@tag,'506|538|540|545') and subfield[matches(@code,'[u]')]]">
		<addlink>
			<xsl:text>$$U</xsl:text>
			<xsl:value-of select="lib:merge(subfield[matches(@code,'[u]')])" />
			<xsl:text>$$D</xsl:text>
			<xsl:choose>
				<xsl:when test="@tag='506'">Access restrictions</xsl:when>
				<xsl:when test="@tag='538'">System details</xsl:when>
				<xsl:when test="@tag='540'">Terms governing use and reproduction</xsl:when>
				<xsl:when test="@tag='545'">Biographical or historical information</xsl:when>
			</xsl:choose>
		</addlink>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[@tag='856' and @ind1='4' and @ind2='2' and subfield[matches(@code,'[u]')]]">
		<addlink>
			<xsl:text>$$U</xsl:text>
			<xsl:value-of select="lib:merge(subfield[matches(@code,'[u]')])" />
			<xsl:text>$$D</xsl:text>
			<xsl:choose>
				<xsl:when test="subfield[matches(@code,'[yz3]')]"> 
					<xsl:value-of select="lib:merge(subfield[matches(@code,'[yz3]')])" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Related online content</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</addlink>
	</xsl:for-each>

	<xsl:for-each select="datafield[@tag='856' and @ind1='4' and @ind2!='2' and subfield[matches(@code,'[u]')] and subfield[@code='3' and matches(lower-case(.),'sample text|publisher description')]]">
		<addlink>
			<xsl:text>$$U</xsl:text>
			<xsl:value-of select="lib:merge(subfield[matches(@code,'[u]')])" />
			<xsl:text>$$D</xsl:text>
			<xsl:value-of select="lib:merge(subfield[matches(@code,'[yz3]')])" />
		</addlink>
	</xsl:for-each>

</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the links/linktouc section -->
<xsl:template name="links-linktouc">
	<xsl:choose>
		<xsl:when test="datafield[@tag='020' and subfield[@code='a']]">
			<linktouc>$$Tworldcat_isbn$$DThis item in WorldCat®</linktouc>
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="oclcid">
				<xsl:call-template name="addata-oclcid" />
			</xsl:variable>
			<xsl:if test="$oclcid/*">
				<linktouc>$$Tworldcat_oclc$$DThis item in WorldCat®</linktouc>
			</xsl:if>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the links/linktofa section -->
<xsl:template name="links-linktofa">
	<xsl:for-each select="datafield[@tag='555']/subfield[@code='u']">
		<linktofa>$$U<xsl:value-of select="." />$$DLink to finding aid</linktofa>
	</xsl:for-each>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the links/linktoexcerpt section -->
<xsl:template name="links-linktoexcerpt">
	<xsl:if test="datafield[@tag='020' and subfield[@code='a']]">
		<linktoexcerpt>$$Tsyndetics_excerpt$$DExcerpt from item</linktoexcerpt>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>
