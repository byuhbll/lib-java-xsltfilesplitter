<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:lib="http://lib.byu.edu/"
	exclude-result-prefixes="lib xd"
	xmlns:saxon="http://icl.com/saxon"
	extension-element-prefixes="saxon">

<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" 
	saxon:character-representation="native;decimal" />


<!-- creates the dedup section -->
<xd:docs>Dedup is currently disabled in Primo by applying a "99" value to t and by running pipes as "FRBR without DEDUP"</xd:docs>
<xsl:template name="dedup">
	<dedup>
		<xsl:call-template name="dedup-t" />
	</dedup>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<xd:docs>Dedup is currently disabled in Primo by applying a "99" value to t and by running pipes as "FRBR without DEDUP"</xd:docs>
<!-- creates the dedup/t section -->
<xsl:template name="dedup-t">
	<t>99</t>
</xsl:template>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


</xsl:stylesheet>
