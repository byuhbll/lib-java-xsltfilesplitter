<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:lib="http://lib.byu.edu/"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:saxon="http://icl.com/saxon"
	exclude-result-prefixes="lib xd"
	extension-element-prefixes="saxon">

<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" 
	saxon:character-representation="native;decimal" />

<!-- creates the facets section -->
<xsl:template name="facets">
	<facets>
		<xsl:call-template name="facets-language" />
		<xsl:call-template name="facets-creationdate" />
		<xsl:call-template name="facets-topic" />
		<xsl:call-template name="facets-collection" />
		<xsl:call-template name="facets-prefilter" />
		<xsl:call-template name="facets-rsrctype" />
		<xsl:call-template name="facets-creatorcontrib" />
		<xsl:call-template name="facets-genre" />
		<xsl:call-template name="facets-toplevel" />
	</facets>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the facets/language section -->
<xd:doc>Language facet from display/language</xd:doc>
<xsl:template name="facets-language">

	<xsl:variable name="display-language">
		<xsl:call-template name="display-language" />
	</xsl:variable>
	
	<xsl:for-each select="tokenize($display-language, ';')">
		<language><xsl:value-of select="." /></language>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the facets/creationdate section -->
<xd:doc>Date facet from display/creationdate</xd:doc>
<xsl:template name="facets-creationdate">
	<xsl:call-template name="display-creationdate" />
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the facets/topic section -->
<xd:doc> <xd:short>Genre facet</xd:short>
  <xd:detail> Genre terms from local and PC PNX  
	<ul>
		<li>From field 600|610|611|630|648|650|651|653|654|655|656|657|658 and related 880</li>
		<li>pulls subfield a-u as topic and v-z as subdivisions</li>
		<li>then merges as topic | subdivision stripping punctuation and adding -- between terms</li>
	</ul>
  </xd:detail>
</xd:doc>
<xsl:template name="facets-topic">

	<xsl:for-each select="datafield[matches(@tag,'600|610|611|630|648|650|651|653|654|655|656|657|658')]
	                    | datafield[@tag='880' and subfield[@code='6' and matches(.,'600|610|611|630')]]">
		<topic>
			<xsl:variable name="topic" select="lib:merge(subfield[matches(@code,'[a-u]')])" />
			<xsl:variable name="subdivisions" select="subfield[matches(@code,'[vxyz]')]" />
			
			<xsl:value-of select="lib:merge(lib:fields-replace($topic|$subdivisions,'\.$|;',''), ' -- ')" />
		</topic>
	</xsl:for-each>

</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the facets/collection section -->
<xd:doc> <xd:short>Collection facet</xd:short>
	<xd:detail> Collection facets allow users to limit results by collection  
		<ul>
			<li>999|m and look up in the SymphonyLibraryCodes table</li>
		</ul>
	</xd:detail>
</xd:doc>

<xsl:template name="facets-collection">
	<xsl:for-each select="datafield[@tag='999']/subfield[@code='m']">
		<xsl:variable name="field-999-m" select="upper-case(.)" />
		<xsl:variable name="type" select="lib:get-from-map($symphonyLibraryCodes,$field-999-m)" />
		
		<collection><xsl:value-of select="$type" /></collection>
		
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the facets/prefilter section -->
<xd:doc><xd:short>Prefilter facet</xd:short>
	<xd:detail> To be implemented</xd:detail>
</xd:doc>
<xsl:template name="facets-prefilter">
	<xsl:variable name="type">
		<xsl:call-template name="display-type" />
	</xsl:variable>

	<prefilter><xsl:value-of select="$type" /></prefilter>

</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the facets/rsrctype section -->
<xd:doc>Resource type facet from display/type. ELMC determined 2013-9-5 that ebook should also get the book facet value</xd:doc>
<xsl:template name="facets-rsrctype">
	<xsl:variable name="type">
		<xsl:call-template name="display-type" />
	</xsl:variable>
	
	<rsrctype><xsl:value-of select="lib:get-from-map($itemTypes, $type, 1, 2)" /></rsrctype>

	<!-- If type is ebook also apply book facet -->
	<xsl:if test="$type = 'ebook'">
		<rsrctype>books</rsrctype>
	</xsl:if>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the facets/creatorcontrib section -->
<xd:doc><xd:short>Creator contributor facet</xd:short>
	<xd:detail>   Allow users to limit serach results by a creator/contributor
	<ul>
		<li>100,700        |a when 100,700 ind 1  = 1,2 and 700 ind2 !=1</li>
		<li>100,111,700,711|a when 100,700 ind 1 != 1,2 and 700 ind2 !=1 replace , with ""</li>
		<li>110,710|ab replace , with ""</li>
	</ul>
  </xd:detail>
</xd:doc>
<xsl:template name="facets-creatorcontrib">

	<xsl:for-each select="datafield[matches(@tag,'100') and matches(@ind1,'[12]')]
						| datafield[matches(@tag,'700') and matches(@ind1,'[12]') and matches(@ind2,'[^1]')]">
		<creatorcontrib><xsl:value-of select="lib:get-name(lib:merge(subfield[matches(@code,'[a]')]))" /></creatorcontrib>
	</xsl:for-each>

	<xsl:for-each select="datafield[matches(@tag,'100') and matches(@ind1,'[^12]')]
						| datafield[matches(@tag,'700') and matches(@ind1,'[^12]') and matches(@ind2,'[^1]')]
						| datafield[matches(@tag,'111|711')]">
		<creatorcontrib><xsl:value-of select="replace(subfield[@code='a'][1],'[.,]$','')" /></creatorcontrib>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'110|710')]">
		<creatorcontrib><xsl:value-of select="replace(lib:merge(subfield[matches(@code,'[ab]')]),'[.,]$','')" /></creatorcontrib>
	</xsl:for-each>

</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the facets/genre section --> 
<xd:doc><xd:short>Genre facet</xd:short>
	<xd:detail>
	genre and topic (Subject in the front end)  are near duplicates
  </xd:detail>
</xd:doc>
  
<xsl:template name="facets-genre">

	<xsl:for-each select="datafield[matches(@tag,'600|610|611|630|648|650|651|653|654|656|657|658')]">
		<genre><xsl:value-of select="lib:merge(lib:fields-replace(subfield[matches(@code,'[v]')],'[.]$',''))" /></genre>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'655')]">
		<genre><xsl:value-of select="lib:merge(lib:fields-replace(subfield[matches(@code,'[a]')],'[.]$',''))" /></genre>
		<genre>
			<xsl:variable name="genre" select="lib:fields-replace(lib:merge(subfield[matches(@code,'[abc]')]),'[.]$','')" />
			<xsl:variable name="subdivisions" select="lib:fields-replace(subfield[matches(@code,'[vxyz]')],'[.]$','')" />
			<xsl:value-of select="lib:merge($genre|$subdivisions,' - ')" />
		</genre>
	</xsl:for-each>

</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the facets/toplevel section-->
<xd:doc><xd:short>Toplevel facet</xd:short>
	<xd:detail>Shows users availibility of items
	<ul>
		<li>when available and type is Online Resource|SFX Resource|MetaLib Resource apply 'online'</li>
		<li>when available and type is Physical item apply 'on_shelf'</li>
	</ul>
	<footer>Last updated: 2013-5-30   By: Eric Nord</footer>
  </xd:detail>
</xd:doc>
<xsl:template name="facets-toplevel">
	
	<xsl:variable name="delcategory"><xsl:call-template name="delivery-delcategory" /></xsl:variable>
	<xsl:variable name="source"><xsl:call-template name="control-sourceid" /></xsl:variable>
	<xsl:for-each select="datafield[@tag='999']">
		
		<xsl:variable name="availability">
			<xsl:choose>
				<xsl:when test="datafield[@tag='999']/subfield[@code='k']">
					<xsl:value-of select="lib:get-from-map($availability, concat(subfield[@code='k'], ' ', subfield[@code='m']))" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="lib:get-from-map($availability, concat(subfield[@code='l'], ' ', subfield[@code='m']))" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:if test="availability!='unavailable' and matches($delcategory,'Online Resource|SFX Resource|MetaLib Resource')"><toplevel>online</toplevel></xsl:if>
		<xsl:if test="$availability!='unavailable' and $delcategory ='Physical Item'"><toplevel>on_shelf</toplevel></xsl:if>
		<xsl:if test="$source ='ebyu'"><toplevel>online</toplevel></xsl:if>
	</xsl:for-each>
	
</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


</xsl:stylesheet>
