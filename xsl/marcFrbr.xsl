<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:lib="http://lib.byu.edu/"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	exclude-result-prefixes="lib xd"
	xmlns:saxon="http://icl.com/saxon"
	extension-element-prefixes="saxon">

<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" 
	saxon:character-representation="native;decimal" />

<!-- creates the frbr section -->
<xd:doc>FRBR combines records that meet certain criteria - this particular implementation does not meet our need and may be replaced int he future</xd:doc>
<xsl:template name="frbr">
	<frbr>
		<xsl:call-template name="frbr-t" />
		<xsl:call-template name="frbr-k1" />
		<xsl:call-template name="frbr-k2" />
		<xsl:call-template name="frbr-k3" />
	</frbr>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the frbr-t section -->
<xd:doc>Disabled FRBR for music scores by applying "99" vs "1"</xd:doc>
<xsl:template name="frbr-t">
	<t>1</t>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the frbr-k1 section -->
<xsl:template name="frbr-k1">
	<xsl:choose>
		<xsl:when test="datafield[@tag='100']">
			<k1>$$K<xsl:value-of select="lib:prepFrbrKey(lib:merge(datafield[@tag='100']/subfield[matches(@code,'[abcdq]')]))" />$$AA</k1>
		</xsl:when>
		<xsl:when test="datafield[@tag='110']">
			<k1>$$K<xsl:value-of select="lib:prepFrbrKey(lib:merge(datafield[@tag='110']/subfield[matches(@code,'[abcdq]')]))" />$$AA</k1>
		</xsl:when>
		<xsl:when test="datafield[@tag='111']">
			<k1>$$K<xsl:value-of select="lib:prepFrbrKey(lib:merge(datafield[@tag='111']/subfield[matches(@code,'[abcdnq]')]))" />$$AA</k1>
		</xsl:when>
		<xsl:otherwise>
			<xsl:for-each select="datafield[@tag='700' and not(subfield[@tag='e' and matches(lower-case(.), 'former owner')])]">
				<k1>$$K<xsl:value-of select="lib:prepFrbrKey(lib:merge(subfield[matches(@code,'[abcdq]')]))" />$$AA</k1>
			</xsl:for-each>
			<xsl:for-each select="datafield[@tag='710' and not(subfield[@tag='e' and matches(lower-case(.), 'former owner')])]">
				<k1>$$K<xsl:value-of select="lib:prepFrbrKey(lib:merge(subfield[matches(@code,'[abcdq]')]))" />$$AA</k1>
			</xsl:for-each>
			<xsl:for-each select="datafield[@tag='711']">
				<k1>$$K<xsl:value-of select="lib:prepFrbrKey(lib:merge(subfield[matches(@code,'[abcdnq]')]))" />$$AA</k1>
			</xsl:for-each>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the frbr-k2 section -->
<xsl:template name="frbr-k2">
	<xsl:variable name="type">
		<xsl:call-template name="display-type" />
	</xsl:variable>
	
	<xsl:for-each select="datafield[@tag='130' and not(subfield[@code='k' and matches(lower-case(.), 'selections|census')])]">
		<k2>
			<xsl:text>$$K</xsl:text>
			<xsl:value-of select="lib:prepFrbrKeyAdvanced(lib:merge(subfield[matches(@code,'[abmnprs]')]), @ind1)" />
			<xsl:text> </xsl:text>
			<xsl:value-of select="$type" />
			<xsl:text>$$ATO</xsl:text>
		</k2>
	</xsl:for-each>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the frbr-k3 section -->
<xsl:template name="frbr-k3">
	<xsl:variable name="type">
		<xsl:call-template name="display-type" />
	</xsl:variable>

	<xsl:for-each select="datafield[@tag='240' and not(matches(lib:merge(subfield[matches(@code,'[ak]')]), 'laws|bills|statutes|public general acts|acts|rules|treaties|census'))]">
		<k3>
			<xsl:text>$$K</xsl:text>
			<xsl:value-of select="lib:prepFrbrKeyAdvanced(lib:merge(subfield[matches(@code,'[admnprs]')]), @ind2)" />
			<xsl:text> </xsl:text>
			<xsl:value-of select="$type" />
			<xsl:text>$$AT</xsl:text>
		</k3>
	</xsl:for-each>

	<xsl:choose>
		<xsl:when test="datafield[@tag=245]">
			<xsl:for-each select="datafield[@tag=245]">
				<k3>
					<xsl:text>$$K</xsl:text>
					<xsl:value-of select="lib:prepFrbrKeyAdvanced(lib:merge(subfield[matches(@code,'[abefgnp]')]), @ind2)" />
					<xsl:text> </xsl:text>
					<xsl:value-of select="$type" />
					<xsl:text>$$AT</xsl:text>
				</k3>
			</xsl:for-each>
		</xsl:when>
		<xsl:when test="datafield[@tag=242]">
			<xsl:for-each select="datafield[@tag=242]">
				<k3>
					<xsl:text>$$K</xsl:text>
					<xsl:value-of select="lib:prepFrbrKeyAdvanced(lib:merge(subfield[matches(@code,'[abfgnp]')]), @ind2)" />
					<xsl:text> </xsl:text>
					<xsl:value-of select="$type" />
					<xsl:text>$$AT</xsl:text>
				</k3>
			</xsl:for-each>
		</xsl:when>
		<xsl:when test="datafield[@tag=246]">
			<xsl:for-each select="datafield[@tag=246]">
				<k3>
					<xsl:text>$$K</xsl:text>
					<xsl:value-of select="lib:prepFrbrKeyAdvanced(lib:merge(subfield[matches(@code,'[abfgnp]')]), '0')" />
					<xsl:text> </xsl:text>
					<xsl:value-of select="$type" />
					<xsl:text>$$AT</xsl:text>
				</k3>
			</xsl:for-each>
		</xsl:when>
		<xsl:when test="datafield[@tag=247]">
			<xsl:for-each select="datafield[@tag=247]">
				<k3>
					<xsl:text>$$K</xsl:text>
					<xsl:value-of select="lib:prepFrbrKeyAdvanced(lib:merge(subfield[matches(@code,'[abfgnp]')]), '0')" />
					<xsl:text> </xsl:text>
					<xsl:value-of select="$type" />
					<xsl:text>$$AT</xsl:text>
				</k3>
			</xsl:for-each>
		</xsl:when>
		<xsl:when test="datafield[@tag=740 and @ind2!='2']">
			<xsl:for-each select="datafield[@tag=740]">
				<k3>
					<xsl:text>$$K</xsl:text>
					<xsl:value-of select="lib:prepFrbrKeyAdvanced(lib:merge(subfield[matches(@code,'[anp]')]), @ind1)" />
					<xsl:text> </xsl:text>
					<xsl:value-of select="$type" />
					<xsl:text>$$AT</xsl:text>
				</k3>
			</xsl:for-each>
		</xsl:when>
	</xsl:choose>

</xsl:template>
</xsl:stylesheet>