<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:lib="http://lib.byu.edu/"
	exclude-result-prefixes="lib"
	xmlns:saxon="http://icl.com/saxon"
	extension-element-prefixes="saxon">

<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" 
	saxon:character-representation="native;decimal" />

<!-- creates the delivery section -->
<xsl:template name="delivery">
	<delivery>
		<xsl:call-template name="delivery-institution" />
		<xsl:call-template name="delivery-delcategory" />
	</delivery>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the delivery-institution section -->
<xsl:template name="delivery-institution">
	<institution><xsl:value-of select="$institution" /></institution>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the delivery-delcategory section -->
<xsl:template name="delivery-delcategory">
	<xsl:choose>
		<xsl:when test="datafield[@tag='035']/subfield[@code='a' and matches(.,'SFX')]">
			<delcategory>SFX Resource</delcategory>
		</xsl:when>
		<xsl:when test="controlfield[@tag='007' and matches(.,'^cr')]
					  | datafield[@tag='856' and @ind1='4' and @ind2!='2' and not(subfield[@code='3'][matches(lower-case(.),'table of contents|book review|sample text|publisher description')])]/subfield[@code='u']">
			<delcategory>Online Resource</delcategory>
		</xsl:when>
		<xsl:when test="controlfield[@tag='007' and matches(.,'^h')]
					 or controlfield[@tag='008'] and datafield[@tag='245']/subfield[@code='h' and matches(lower-case(.),'micro')]
					 or matches($fmt,'BK|SE|MU|MX|MP|VM') and controlfield[@tag='008' and matches(substring(.,24,1),'[abc]')]">
			<delcategory>Microform</delcategory>
		</xsl:when>
		<xsl:otherwise>
			<delcategory>Physical Item</delcategory>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


</xsl:stylesheet>
