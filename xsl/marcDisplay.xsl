<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	exclude-result-prefixes="lib map xd"
	xmlns:lib="http://lib.byu.edu/"
	xmlns:map="urn:schemas-microsoft-com:office:spreadsheet"
	xmlns:saxon="http://icl.com/saxon"
	extension-element-prefixes="saxon">

<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" 
	saxon:character-representation="native;decimal" />
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the display section -->
<xd:doc>The Display section is used by Primo and ScholarSearch for brief and detail result pages</xd:doc>
	<xsl:template name="display">
		<display>
			<xsl:call-template name="display-type" />
			<xsl:call-template name="display-title" />
			<xsl:call-template name="display-creator" />
			<xsl:call-template name="display-contributor" />
			<xsl:call-template name="display-edition" />
			<xsl:call-template name="display-publisher" />
			<xsl:call-template name="display-creationdate" />
			<xsl:call-template name="display-format" />
			<xsl:call-template name="display-ispartof" />
			<xsl:call-template name="display-identifier" />
			<xsl:call-template name="display-subject" />
			<xsl:call-template name="display-description" />
			<xsl:call-template name="display-language" />
			<xsl:call-template name="display-relation" />
			<xsl:call-template name="display-source" />
			<xsl:call-template name="display-vertitle" />
			<xsl:call-template name="display-unititle" />
			<xsl:call-template name="display-availlibrary" />
			<xsl:call-template name="display-lds01" /> <!-- Record id          --> 	
			<xsl:call-template name="display-lds02" /> <!-- Table of contents  -->
			<xsl:call-template name="display-lds03" /> <!-- Series info        -->	
			<xsl:call-template name="display-lds05" /> <!-- Notes fields       -->
			<xsl:call-template name="display-lds06" /> <!-- continues	       -->
			<xsl:call-template name="display-lds07" /> <!-- continuedBy	       -->		
		</display>
	</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the display/type section -->
<xd:doc> <xd:short>Item type defines thumbnails, item type facet, and search drop down</xd:short>
  <xd:detail><p>Providing clear access to patrons involves differentiating between various item types. 
  				Here we break item types in several ways using the lrd, 655, Symphony item type aka 999|t, and other fields</p>
	<ul>
		<li>Dissertation: 502 exists</li>
		<li>Book, Map, Score, Audio: from ldr <code>Add ldr format mapping table link</code></li>
		<li>Database, Text Resource, Book, Website: 008 position 21 d,l,m,w</li>
		<li>Image, Video: 008 position 33 'klnopqst', 'fmv'</li>
		<li>Audio, Website: 008 position 26 h,j</li>
		<li>Other: All records that make it this far get item type of Other</li>
	</ul>
  </xd:detail>
</xd:doc>
<xsl:template name="display-type">

	<!-- get field 999/t from marc record -->
	<xsl:variable name="d-999-t" select="datafield[@tag='999']/subfield[@code='t'][1]" />
	
	<!-- get the item type from the mapping table -->
	<xsl:variable name="type" select="lib:get-from-map($symphonyTypes, $d-999-t)" />
	<type>
		<xsl:choose>
			<!-- Streaming audio -->
			<xsl:when test="datafield[@tag='999']/subfield[@code='t' and matches(.,'STREAMING')] 
			                and datafield[@tag='655']/subfield[@code='a' 
			                and matches(.,'Live sound recordings\.|Streaming audio files\.|Streaming audio\.|Field recordings\.|Monologues with music (Orchestra)')]">streamingaudio</xsl:when>
			<!-- Type from symphony item type -->
			<xsl:when test="$type!=''"><xsl:value-of select="$type" /></xsl:when>
			<xsl:when test="datafield[@tag='502']">dissertation</xsl:when>
			
			<xsl:when test="lib:validate-fmt('MP')">map</xsl:when>
			<xsl:when test="lib:validate-fmt('SC')">score</xsl:when>
			<xsl:when test="lib:validate-fmt('MU')">audio</xsl:when>
			<xsl:when test="lib:validate-fmt('SE')">
				<xsl:variable name="c008-21" select="substring(controlfield[@tag='008'][1], 21, 1)" />
				<xsl:choose>
					<xsl:when test="$c008-21 = 'd'">database</xsl:when>
					<xsl:when test="$c008-21 = 'l'">text_resource</xsl:when>
					<xsl:when test="$c008-21 = 'm'">book</xsl:when>
					<xsl:when test="$c008-21 = 'w'">website</xsl:when>
					<xsl:otherwise>journal</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="lib:validate-fmt('VM')"> 
				<xsl:variable name="c008-33" select="substring(controlfield[@tag='008'][1], 33, 1)" />
				<xsl:choose>
					<xsl:when test="contains('klnopqst', $c008-33)">image</xsl:when>
					<xsl:when test="contains('fmv', $c008-33)">video</xsl:when>
					<xsl:otherwise>other</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="lib:validate-fmt('CF')"> 
				<xsl:variable name="c008-26" select="substring(controlfield[@tag='008'][1], 26, 1)" />
				<xsl:choose>
					<xsl:when test="contains('de', $c008-26)">text_resource</xsl:when>
					<xsl:when test="$c008-26 = 'h'">audio</xsl:when>
					<xsl:when test="$c008-26 = 'j'">website</xsl:when>
					<xsl:otherwise>other</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="lib:validate-fmt('BK')">book</xsl:when>
			<xsl:otherwise>other</xsl:otherwise>
		</xsl:choose>
	</type>
</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the display/title section -->
<xd:doc> <xd:short>Title</xd:short>
  <xd:detail><p>We attempt to display as much title information as is available in the MARC record. Title search field are handeled differently as users aren't often aware of the full description</p>
	<ul>
		<li>If serial and 130 exists, Title = 130|adfklmnoprs</li>
		<li>Otherwise, Title = 245|abfgknp, and remove all punctuation /:;=</li>
		<li>Removed 800|t as a series statement to the end of the title due to behavior like  "Year ... at Hogwarts" good info but confusing to users</li>
		<li>Records with no title throw error in Primo harvest</li>
	</ul>
  </xd:detail>
</xd:doc>
	<xsl:template name="display-title">

		<title>
			<xsl:choose>
				<xsl:when test="lib:validate-fmt('SE') and datafield[@tag='130']">
					<xsl:value-of select="datafield[@tag='130']/subfield[matches(@code,'[adfklmnoprs]')]" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="replace(lib:merge(datafield[@tag='245']/subfield[matches(@code,'[abfgknp]')]),'[/:;=]$','')" />
				</xsl:otherwise>
			</xsl:choose>
		</title>
		
	</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the display/creator section -->
<xd:doc> <xd:short>Creator</xd:short>
  <xd:detail><p>Lists creators of the work</p>
	<ul>
		<li>Get name from 100|aq or related 880|aq then add bcdeju</li>
		<li>Get name from 110,111 or related 880 </li>
	</ul>
  </xd:detail>
</xd:doc>
<xsl:template name="display-creator">
	
	<xsl:variable name="creator">	

		<xsl:for-each select="datafield[@tag='100'] | datafield[@tag='880' and subfield[@code='6' and matches(.,'100')]]">
		    <!-- get name and fuller form of name -->
		    <xsl:variable name="name" select="lib:wrap(replace(normalize-space(concat(lib:get-name(subfield[@code='a'][1]), ' ', subfield[@code='q'][1])),',$',''))" />
		    <!-- combine name with other data associated with creator and remove ending dashes and commas -->
			<creator><xsl:value-of select="lib:merge(lib:fields-replace($name|subfield[matches(@code,'[bcdeju]')],'[,-]$',''),', ')" /></creator>
		</xsl:for-each>

		<xsl:for-each select="datafield[matches(@tag,'110|111')] | datafield[@tag='880' and subfield[@code='6' and matches(.,'110|111')]]">
			<creator><xsl:value-of select="lib:merge(subfield[matches(@code,'[abd]')])" /></creator>
		</xsl:for-each>
		
	</xsl:variable>
	
	<creator>
		<xsl:value-of select="lib:merge($creator/*, '; ')" />
	</creator>
	
</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the display/contributor section -->
<xd:doc> <xd:short>Contributors</xd:short>
  <xd:detail><p>Lists those who contributed to the work one way or antoher but are not creators</p>
	<ul>
		<li> 700|aq and related 880 then adds |bcdeju</li>
		<li>Same for 710|abcde, 711|abcdn, 245|c </li>
	</ul>
  </xd:detail>
</xd:doc>
<xsl:template name="display-contributor">
	
	<xsl:variable name="contributor">
	
		<xsl:for-each select="datafield[@tag='700' and @ind2!='2']
		                    | datafield[@tag='880' and @ind2!='2' and subfield[@code='6' and matches(.,'700')]]">
		    <!-- get name and fuller form of name -->
		    <xsl:variable name="name" select="lib:wrap(replace(normalize-space(concat(lib:get-name(subfield[@code='a'][1]), ' ', subfield[@code='q'][1])),',$',''))" />
		    <!-- combine name with other data associated with contributor and remove ending dashes and commas -->
			<contributor><xsl:value-of select="lib:merge(lib:fields-replace($name|subfield[matches(@code,'[bcdeju]')],'[,-]$',''),', ')" /></contributor>
		</xsl:for-each>
		
		<xsl:for-each select="datafield[@tag='710' and @ind2!='2']
		                    | datafield[@tag='880' and @ind2!='2' and subfield[@code='6' and matches(.,'710')]]">
			<contributor><xsl:value-of select="lib:merge(subfield[matches(@code,'[abcde]')])" /></contributor>
		</xsl:for-each>
		
		<xsl:for-each select="datafield[@tag='711' and @ind2!='2']
		                    | datafield[@tag='880' and @ind2!='2' and subfield[@code='6' and matches(.,'711')]]">
			<contributor><xsl:value-of select="lib:merge(subfield[matches(@code,'[abcdn]')])" /></contributor>
		</xsl:for-each>
		
		<xsl:for-each select="datafield[matches(@tag,'245')]
 	                    | datafield[@tag='880' and subfield[@code='6' and matches(.,'245')]]">
			<contributor><xsl:value-of select="lib:merge(subfield[matches(@code,'[c]')])" /></contributor>
		</xsl:for-each>
		
	</xsl:variable>
	
	<contributor>
		<xsl:value-of select="lib:merge($contributor/*, '; ')" />
	</contributor>
	
</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the display/edition section -->
<xd:doc> <xd:short>Edition</xd:short>
  <xd:detail><p>Edition information is important to students that need a prticular edition of a textbook etc.</p>
	<ul>
		<li>250|ab and related 880</li>
	</ul>
  </xd:detail>
</xd:doc>
<xsl:template name="display-edition">

	<xsl:variable name="edition">
		<xsl:for-each select="datafield[@tag='250'] | datafield[@tag='880' and subfield[@code='6' and matches(.,'250')]]">
			<edition><xsl:value-of select="lib:merge(subfield[matches(@code,'[ab]')])" /></edition>
		</xsl:for-each>
	</xsl:variable>
	
	<edition>
		<xsl:value-of select="lib:merge($edition/*,'; ')" />
	</edition>

</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the display/publisher section -->
<xd:doc> <xd:short>Publisher</xd:short>
  <xd:detail>
	<ul>
		<li>502|a and related 880</li>
		<li>880|abc if 880|6 = 260,264</li>
		<li>260|abc or 264|abc if no 502</li>
	</ul>
  </xd:detail>
</xd:doc>
<xsl:template name="display-publisher">

	<xsl:variable name="publisher">
		<xsl:for-each select="datafield[@tag='502'] | datafield[@tag='880' and subfield[@code='6' and matches(.,'502')]]">
			<publisher><xsl:value-of select="lib:merge(subfield[@code='a'])" /></publisher>
		</xsl:for-each>
		
		<xsl:for-each select="datafield[@tag='880' and subfield[@code='6' and matches(.,'260|264')]]">
			<publisher><xsl:value-of select="replace(lib:merge(subfield[matches(@code,'[ab]')]),'[\[\]\(\)]|[:,=]+$','')" /></publisher>
		</xsl:for-each>
		
		<xsl:if test="not(datafield[@tag='502'])">
			<xsl:for-each select="datafield[matches(@tag,'260|264')]">
				<publisher><xsl:value-of select="replace(lib:merge(subfield[matches(@code,'[ab]')]),'[\[\]\(\)]|[:,=]+$','')" /></publisher>
			</xsl:for-each>
		</xsl:if>
	</xsl:variable>

	<publisher>
		<xsl:value-of select="lib:merge($publisher/*,'; ')" />
	</publisher>

</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the display/creationdate section -->
<xd:doc> <xd:short>Creation date</xd:short>
	<xd:detail>Pulls from lib:dates</xd:detail>
</xd:doc>
<xsl:template name="display-creationdate">

	<creationdate><xsl:value-of select="lib:dates(.)[1]" /></creationdate>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the display/format section -->
<xd:doc> <xd:short>Format</xd:short>
	<xd:detail>Lists the access format, ie. html, pdf, etc... Pulls from 300 or 340</xd:detail>
</xd:doc>
<xsl:template name="display-format">
	<xsl:variable name="format">
		<xsl:for-each select="datafield[matches(@tag,'300|340')]">
			<format><xsl:copy-of select="lib:merge(subfield[matches(@code,'[^0-9]')])" /></format>
		</xsl:for-each>
	</xsl:variable>
		
	<format>
		<xsl:value-of select="lib:merge($format/*,'.; ')" />
	</format>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the display/ispartof section -->
<xd:doc> <xd:short>ispartof may not currently be in use in the front end - didn't find examples of it's usage in brief tests</xd:short>
	<xd:detail> Link item to host item for instance - byu1008032	ispartof = Isaac Asimov's science fiction, vol.10, no.2 (Feb. 1986)
	<p> 773|a-w and related 880</p></xd:detail>
</xd:doc>
<xsl:template name="display-ispartof">

	<xsl:variable name="ispartof">
		<xsl:for-each select="datafield[@tag='773']
		                    | datafield[@tag='880' and subfield[@code='6' and matches(.,'773')]]">
			<ispartof><xsl:value-of select="lib:merge(subfield[matches(@code,'[^xyz0-9]')])" /></ispartof>
		</xsl:for-each>
	</xsl:variable>
	
	<ispartof>
		<xsl:value-of select="lib:merge($ispartof/*,'.; ')" />
	</ispartof>

</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the display/identifier section -->
<xd:doc> <xd:short>Identifer - ISBN, ISSN, ISMN</xd:short>
	<xd:detail>Provides idetification information
	<ul>
		<li>ISBN from 020|a</li>
		<li>ISSN from 022|a</li>
		<li>ISMN from 024|a</li>
	</ul>
	</xd:detail>
</xd:doc>
<xsl:template name="display-identifier">
	<xsl:variable name="identifier">
		<xsl:for-each select="datafield[@tag='020']/subfield[@code='a']">
			<identifier>$$CISBN$$V<xsl:value-of select="." /></identifier>
		</xsl:for-each>
		<xsl:for-each select="datafield[@tag='022']/subfield[@code='a']">
			<identifier>$$CISSN$$V<xsl:value-of select="." /></identifier>
		</xsl:for-each>
		<xsl:for-each select="datafield[@tag='024']/subfield[@code='a']">
			<identifier>$$CISMN$$V<xsl:value-of select="." /></identifier>
		</xsl:for-each>
	</xsl:variable>
	
	<identifier>
		<xsl:value-of select="lib:merge($identifier/*, ';')" />
	</identifier>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the display/subject section -->
<xd:doc> <xd:short>Subjects</xd:short>
	<xd:detail>
		<p>600|610|611|630|648|653|654|655|656|657|658|650|651 subfield a-u and 880 related to 600|610|611|630 
		<br/>  subdivisions from subfields v-z
		</p>
	</xd:detail>
</xd:doc>
<xsl:template name="display-subject">
	<xsl:variable name="subject">
		<xsl:for-each select="datafield[matches(@tag,'600|610|611|630|648|653|654|655|656|657|658|650|651')]
		                    | datafield[@tag='880' and subfield[@code='6' and matches(.,'600|610|611|630')]]">
		    
		    <xsl:variable name="subject" select="lib:merge(subfield[matches(@code,'[a-u]')])" />
		    <xsl:variable name="subdivisions" select="subfield[matches(@code,'[v-z]')]" />
		    
			<subject><xsl:value-of select="lib:merge(lib:fields-replace($subject|$subdivisions,'\.$|;',''),' -- ')" /></subject>
		</xsl:for-each>
	</xsl:variable>
	
	<subject>
		<xsl:value-of select="lib:merge($subject/*, '; ')" />
	</subject>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

		
<!-- creates the display/description section -->
<xd:doc> <xd:short>Description</xd:short>
	<xd:detail>
		<ul>
			<li>520|ab and related 880</li>
		</ul>
	</xd:detail>
</xd:doc>
<xsl:template name="display-description">
	
	<xsl:for-each select="datafield[matches(@tag,'520')] | datafield[@tag='880' and subfield[@code='6' and matches(.,'520')]]">
		<description><xsl:value-of select="lib:merge(subfield[matches(@code,'[ab]')])" /></description>
	</xsl:for-each>
</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the display/language section -->
<xd:doc><xd:short>Language</xd:short>
	<xd:detail>
		<ul>
			<li>If 008 position 36 +3 != zxx then use the 3 letter language code </li>
			<li>041|ade, every 3 characters</li>
			<li>merge all </li>
		</ul>
	</xd:detail>
</xd:doc>
<xsl:template name="display-language">

	<xsl:variable name="lang" select="substring(controlfield[@tag='008'][1], 36, 3)" />
	
	<xsl:variable name="language">
		<xsl:if test="$lang != 'zxx'"> 
			<language><xsl:value-of select="$lang" /></language>
		</xsl:if>
		
		<xsl:for-each select="lib:split-interval(lib:merge(datafield[@tag='041']/subfield[matches(@code,'[ade]')],''),3)"> 
			<language><xsl:value-of select="." /></language>
		</xsl:for-each>
	</xsl:variable>
	
	<language>
		<xsl:value-of select="lib:merge(lib:dedup($language/*), ';')" />
	</language>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the display/relation section -->
<xd:doc><xd:short>realtion doesn not seem to be in use in the front end in brief tests</xd:short>
	<xd:detail>
		<ul>
			<li>400|410|411|440|490 all subfields a-z and related 880</li>
			<li>800|810|811|830|840|a-z and related 880 fields</li>
			<li>Series when 780|all but wxy67 when ind1 !=1 and related 880</li>
			<li>Earlier_title when 785|all but wxy67 when ind1 !=1 and related 880</li>
		</ul>
	</xd:detail>
</xd:doc>
<xsl:template name="display-relation">
	<xsl:variable name="relation">
		<xsl:for-each select="datafield[matches(@tag,'400|410|411|440|490')] | datafield[@tag='880' and subfield[@code='6' and matches(.,'400|410|411|440')]] | datafield[@tag='880' and @ind1='0' and subfield[@code='6' and matches(.,'490')]]">
			<relation><xsl:value-of select="lib:merge(subfield[matches(@code,'[a-z]')])" /></relation>
		</xsl:for-each>
		
		<xsl:for-each select="datafield[matches(@tag,'800|810|811|830|840')]">
			<relation><xsl:value-of select="lib:merge(subfield[matches(@code,'[a-z]')])" /></relation>
		</xsl:for-each>
		
		<xsl:for-each select="datafield[@tag='880' and subfield[@code='6' and matches(.,'800|810|811|830|840')]]">
			<relation><xsl:value-of select="lib:merge(subfield[matches(@code,'[468]')])" /></relation>
		</xsl:for-each>
	</xsl:variable>
	
	<relation>
		<xsl:if test="$relation/*">
			<xsl:text>$$Cseries</xsl:text>
		</xsl:if>
		
		<xsl:call-template name="lib:merge">
			<xsl:with-param name="fields">
				<xsl:for-each select="$relation/*">
					<xsl:if test="not(.='')">
						<xsl:text>$$V</xsl:text>
						<xsl:value-of select="." />
					</xsl:if>
				</xsl:for-each>
			</xsl:with-param>
			<xsl:with-param name="delimiter" select="'; '" />
		</xsl:call-template>
	</relation>
	
	<xsl:variable name="relation">
		<xsl:for-each select="datafield[@tag='780' and matches(@ind1,'[^1]')]
		                    | datafield[@tag='880' and matches(@ind1,'[^1]') and subfield[@code='6' and matches(.,'780')]]">
			<relation><xsl:value-of select="lib:merge(subfield[matches(@code,'[^wxy67]')])" /></relation>
		</xsl:for-each>
	</xsl:variable>
	
	<relation>
		<xsl:if test="$relation/*">
			<xsl:text>$$Cearlier_title</xsl:text>
		</xsl:if>
		
		<xsl:call-template name="lib:merge">
			<xsl:with-param name="fields">
				<xsl:for-each select="$relation/*">
					<xsl:if test="not(.='')">
						<xsl:text>$$V</xsl:text>
						<xsl:value-of select="." />
					</xsl:if>
				</xsl:for-each>
			</xsl:with-param>
			<xsl:with-param name="delimiter" select="'; '" />
		</xsl:call-template>
	</relation>
	
	<xsl:variable name="relation">
		<xsl:for-each select="datafield[@tag='785' and matches(@ind1,'[^1]')]
		                    | datafield[@tag='880' and matches(@ind1,'[^1]') and subfield[@code='6' and matches(.,'785')]]">
			<relation><xsl:value-of select="lib:merge(subfield[matches(@code,'[^wxy67]')])" /></relation>
		</xsl:for-each>
	</xsl:variable>
	
	<relation>
		<xsl:if test="$relation/*">
			<xsl:text>$$Cearlier_title</xsl:text>
		</xsl:if>
		
		<xsl:call-template name="lib:merge">
			<xsl:with-param name="fields">
				<xsl:for-each select="$relation/*">
					<xsl:if test="not(.='')">
						<xsl:text>$$V</xsl:text>
						<xsl:value-of select="." />
					</xsl:if>
				</xsl:for-each>
			</xsl:with-param>
			<xsl:with-param name="delimiter" select="'; '" />
		</xsl:call-template>
	</relation>
	
</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the display/source section -->
<xd:doc><xd:short>Source id - byu, byuh, ebyu, ldsbc, law, etc...</xd:short>
	<xd:detail>Pulls the sourceid from the marcControl.xsl template</xd:detail>
</xd:doc>
<xsl:template name="display-source">
	<xsl:variable name="sourceid">
		<xsl:call-template name="control-sourceid" />
	</xsl:variable>
	
	<source>
		<xsl:value-of select="$sourceid" />
	</source>
</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the display/vertitle section -->
<xd:doc><xd:short>Vernacular Title</xd:short>
	<xd:detail> <p>Provides alternate language title information</p>
		<ul>
			<li>880|abfgknp when related to 245</li>
		</ul>
	</xd:detail>
</xd:doc>
<xsl:template name="display-vertitle">
	<xsl:for-each select="datafield[matches(@tag,'880') and subfield[@code='6'] and matches(.,'245')]">
		<vertitle>
			<xsl:value-of select="lib:fields-replace(subfield[matches(@code,'[abfgknp]')],'/:;=$','')" />
		</vertitle>
	</xsl:for-each>
</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
 
<!-- creates the display/unititle section -->
<xd:doc><xd:short>Uniform Title</xd:short>
	<xd:detail><p>Displays Uniform Title in the details page</p>
		<ul>
			<li>130|admnprs or 240|admnprs</li>
		</ul>
	</xd:detail>
</xd:doc>
<xsl:template name="display-unititle">
	<unititle><xsl:value-of select="datafield[matches(@tag,'130|240')][1]/subfield[matches(@code,'[admnprs]')]" /></unititle>
</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the display/availlibrary section -->
<xd:doc><xd:short>Availability</xd:short>
	<xd:detail>
		
		<ul>
		<li>$$I = institution</li>
		<li>$$L = takes 999|m and provides </li>
		<li>$$1 = takes 999|l and provides location description (special case used for faculty use items)</li>
		<li>$$2 = takes 999|a and provides Call number</li>
		<li>$$S = takes 999|k combined with 999|m to provide availability message (this is usually taken from real time web service call and not from this field)</li>
	</ul>
		
	</xd:detail>
</xd:doc>
<xsl:template name="display-availlibrary">
	<xsl:for-each select="datafield[@tag='999']">
		<availlibrary>
			<xsl:text>$$I</xsl:text>
			<xsl:value-of select="$institution" />
			<xsl:text>$$L</xsl:text>
			<xsl:value-of select="lib:get-from-map($symphonyLibraryCodes,subfield[@code='m'])" />
			<xsl:text>$$1</xsl:text>
			<xsl:value-of select="lib:get-from-map($symphonyLocations,subfield[@code='l'])" />	
			<xsl:text>$$2</xsl:text>
			<xsl:value-of select="subfield[@code='a']" />
			<xsl:text>$$S</xsl:text>
			<xsl:value-of select= "datafield[@tag='999']/subfield[@code='k']"/>
			<!-- sets up availability status -->
			<xsl:variable name="availability">
	            <xsl:choose>
	                    <xsl:when test="datafield[@tag='999']/subfield[@code='k']">
	                    	<xsl:value-of select="datafield[@tag='999']/subfield[@code='k']"/>
	                    	<xsl:value-of select="lib:get-from-map($availability, concat(subfield[@code='k'], ' ', subfield[@code='m']))" />
	                    </xsl:when>
	                    <xsl:otherwise>
	                            <xsl:value-of select="lib:get-from-map($availability, concat(subfield[@code='l'], ' ', subfield[@code='m']))" />
	                    </xsl:otherwise>
	            </xsl:choose>
            </xsl:variable>
            <xsl:choose>
                    <xsl:when test="$availability!=''">
                            <xsl:value-of select="$availability" />
                    </xsl:when>
                    <xsl:otherwise>
                            <xsl:text>check_holdings</xsl:text>
                    </xsl:otherwise>
            </xsl:choose>
		</availlibrary>
	</xsl:for-each>
	
</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the lds01 section -->
<xd:doc><xd:short>recordid - for example byu108959</xd:short>
	<xd:detail>Get recordid from control-recordid</xd:detail>
</xd:doc>
<xsl:template name="display-lds01">
	
	<xsl:variable name="recordid">
		<xsl:call-template name="control-recordid" />
	</xsl:variable>

	<lds01><xsl:value-of select="$recordid" /></lds01>
	
</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the lds02 section: table of contents - previously in the description section -->
<xd:doc><xd:short>Table of Contents Notes and other added entries</xd:short>
	<xd:detail>This was originally in the display/description section
		<ul>
			<li>880|a realted to the 505</li>
			<li>505|at if exists</li>
			<li>otherwise 700,710|bcdemnopst when ind2=2 </li>
			<li>and 711|acdenpqst when ind2=2</li>
		</ul>	
	</xd:detail>
</xd:doc>
<xsl:template name="display-lds02">
	
	<xsl:for-each select="datafield[@tag='880' and subfield[@code='6' and matches(.,'505')]]">
		<lds02><xsl:value-of select="lib:merge(subfield[@code='a]'])" /></lds02>
	</xsl:for-each>
		
	<xsl:choose>
		<xsl:when test="datafield[@tag='505']/subfield[matches(@code,'[at]')]">
			<lds02><xsl:value-of select="lib:merge(datafield[@tag='505']/subfield[matches(@code,'[at]')])" /></lds02>
		</xsl:when>
		<xsl:otherwise>
			<xsl:for-each select="datafield[matches(@tag,'700|710') and @ind2='2']">
				<lds02><xsl:value-of select="lib:merge(subfield[matches(@code,'[abcdemnopst]')])" /></lds02>
			</xsl:for-each>
			
			<xsl:for-each select="datafield[@tag='711' and @ind2='2']">
				<lds02><xsl:value-of select="lib:merge(subfield[matches(@code,'[acdenpqst]')])" /></lds02>
			</xsl:for-each>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- creates the lds03 section: series -->
<xd:doc><xd:short>Series statements from Symphony</xd:short>
	<xd:detail>
		<ul>
			<li>440,800,810,811,830|abt and related 880</li>
			<li>TODO: Add 490 with ind=0 as they have not been indexed and may be the only existing series data - the data is messy however more testing needed</li>
			<li>otherwise 490|abt</li>
		</ul>	
	</xd:detail>
</xd:doc>
<xsl:template name="display-lds03">
	
	<xsl:variable name="series">
		<xsl:for-each select="datafield[matches(@tag,'800|810|811')] | datafield[@tag='880' and subfield[@code='6' and matches(.,'800|810|811')]] | datafield[@tag='880' and @ind1='0' and subfield[@code='6' and matches(.,'800|810|811|830')]]">
			<lds03><xsl:value-of select="lib:merge(subfield[matches(@code,'[bt]')])" /></lds03>
		</xsl:for-each>
	
		<xsl:for-each select="datafield[matches(@tag,'440|830')] | datafield[@tag='880' and subfield[@code='6' and matches(.,'440|830')]] | datafield[@tag='880' and @ind1='0' and subfield[@code='6' and matches(.,'440|830')]]">
			<lds03><xsl:value-of select="lib:merge(subfield[matches(@code,'[a]')])" /></lds03>
		</xsl:for-each>
	</xsl:variable>
	
	<lds03>
		<xsl:call-template name="lib:merge">
			<xsl:with-param name="fields">
				<xsl:for-each select="$series/*">
					<xsl:if test="not(.='')">
						<xsl:text> </xsl:text>
						<xsl:value-of select="replace(.,'[\[\]\(\)]|[:;.,=]+$','')" />
					</xsl:if>
				</xsl:for-each>
			</xsl:with-param>
			<xsl:with-param name="delimiter" select="' '" />
		</xsl:call-template>
	</lds03>
</xsl:template>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the lds05 section: these are all the notes fields from symphony -->
<xd:doc><xd:short>Determines notes section</xd:short>
	<xd:detail>Pulls all note fields approved to be end user facing</xd:detail>
</xd:doc>
<xsl:template name="display-lds05">
	
	<!-- if ind1 = 8 then don't display the note. -->
	<!-- 505 and 520 are already in the description -->
	<!-- 502 is in publisher -->
	<xsl:for-each select="datafield[matches(@tag, '500|501|504|506|507|508|510|511|513|514|515|516|518|521|522|524|525|526|530|533|534|535|538|540|542|544|545|546|547|550|552|555|556|561|562|563|565|567|580|581|585|586|590|591|592|593|594|595|597|598|599') and not(@ind1='8')]">
		<lds05>
		<xsl:choose>
			<xsl:when test="datafield[matches(@tag,'852')]">
				<xsl:text>$$LHoldings</xsl:text>
				<xsl:text>$$V</xsl:text>
				<xsl:value-of select="./subfield[matches(@code,'[z]')]" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>$$L</xsl:text>
				<xsl:value-of select="lib:get-notes-from-map(@tag, @ind1)" />
				<xsl:text>$$V</xsl:text>
				<xsl:value-of select="./subfield[matches(@code,'[abc]')]" />
			</xsl:otherwise>
		</xsl:choose>
		</lds05>
	</xsl:for-each>	
</xsl:template>

<!-- creates the lds06 section -->
<xd:doc>continues. Show preceeding enrty for this work</xd:doc>
<xsl:template name="display-lds06">
	<xsl:for-each select="datafield[matches(@tag,'780')]">
		<lds06><xsl:value-of select="lib:merge(subfield[matches(@code,'[^uwxyz0-9]')])"/></lds06>
	</xsl:for-each>
</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the lds07 section -->
<xd:doc>continued by. Show succeeding enrty for this work</xd:doc>
<xsl:template name="display-lds07">
	<xsl:for-each select="datafield[matches(@tag,'785')]">
		<lds07><xsl:value-of select="lib:merge(subfield[matches(@code,'[^uwxyz0-9]')])"/></lds07>
	</xsl:for-each>	
</xsl:template>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

</xsl:stylesheet>
