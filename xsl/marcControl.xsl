<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:lib="http://lib.byu.edu/"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	exclude-result-prefixes="lib xd"
	xmlns:saxon="http://icl.com/saxon"
	extension-element-prefixes="saxon">

<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" 
	saxon:character-representation="native;decimal" />


<!-- creates the control section -->
<xd:doc>Control section handles source system data and record ids providing unique identifiers across disparate data sources</xd:doc>
<xsl:template name="control">
	<control>
		<xsl:call-template name="control-sourcerecordid" />
		<xsl:call-template name="control-sourceid" />
		<xsl:call-template name="control-recordid" />
		<xsl:call-template name="control-sourceformat" />
		<xsl:call-template name="control-sourcesystem" />
	</control>
</xsl:template>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the control/sourcerecordid section -->
<xd:doc>Lists the record id provided by the source record</xd:doc>
<xsl:template name="control-sourcerecordid">
	<sourcerecordid>
		<xsl:choose>
			<xsl:when test="controlfield[@tag='002']">
				<xsl:value-of select="replace(controlfield[@tag='002'][1],'^u?(.+)','$1')" /> 
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="controlfield[@tag='001'][1]" /> 
			</xsl:otherwise>
		</xsl:choose>
	</sourcerecordid>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the control/sourceid section -->
<xd:doc>Lists the source id demarking separate metadata sources </xd:doc>
<xsl:template name="control-sourceid">
	<sourceid><xsl:value-of select="$source" /></sourceid>
</xsl:template>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the control/recordid section -->
<xd:doc>Record id generated by combining source id + source record id </xd:doc>
<xsl:template name="control-recordid">
	<xsl:variable name="sourcerecordid">
		<xsl:call-template name="control-sourcerecordid" />
	</xsl:variable>
	
	<recordid>
		<xsl:value-of select="$source"/>
		<xsl:value-of select="$sourcerecordid" />
	</recordid>
</xsl:template>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the control/sourceformat section -->
<xd:doc>Source record format. Currently hard coded to MARC21</xd:doc>
<xsl:template name="control-sourceformat">
	<sourceformat>MARC21</sourceformat>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- creates the control/sourcesystem section -->
<xd:doc>Source system. Currently hard coded to Symphony</xd:doc>
<xsl:template name="control-sourcesystem">
	<sourcesystem>symphony</sourcesystem>
</xsl:template>

</xsl:stylesheet>