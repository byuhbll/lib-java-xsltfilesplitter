<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:lib="http://lib.byu.edu/"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	exclude-result-prefixes="lib xd"
	xmlns:saxon="http://icl.com/saxon"
	extension-element-prefixes="saxon">

<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" 
	saxon:character-representation="native;decimal" />

<!-- creates the ranking section -->
<xd:doc>Ranking allows modification to the boosting levels of particular record sets</xd:doc>
<xsl:template name="ranking">
	<ranking>
		<xsl:call-template name="ranking-booster1" />
		<xsl:call-template name="ranking-booster2" />
	</ranking>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the ranking/booster1 section -->
<xd:doc>booster1 changes ranking of a record. booster1 has a mutiplicative effect on the rank of a record or record set it is currently set to 1 or no boost</xd:doc>
<xsl:template name="ranking-booster1">
	<booster1>1</booster1>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the ranking/booster2 section -->
<xd:doc>booster2 has not yet been implemented by Exlibris</xd:doc>
<xsl:template name="ranking-booster2">
	<booster2>1</booster2>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


</xsl:stylesheet>
