<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:lib="http://lib.byu.edu/"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	exclude-result-prefixes="lib xd"
	xmlns:saxon="http://icl.com/saxon"
	extension-element-prefixes="saxon">



<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" 
	saxon:character-representation="native;decimal" />

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search section -->
<xd:doc>Search section is loaded to the Jaguar-Lucene engine for search indexing. 
        Results from the Lucene search engine are then enriched by the recordId information in the Oracle DB. 
        Often several variants of the display fields are used to cover varied user search input.</xd:doc>
<xsl:template name="search">
	<search>
		<xsl:call-template name="search-creatorcontrib" />
		<xsl:call-template name="search-title" />
		<xsl:call-template name="search-description" />
		<xsl:call-template name="search-subject" />
		<xsl:call-template name="search-sourceid" />
		<xsl:call-template name="search-recordid" />
		<xsl:call-template name="search-general" />
		<xsl:call-template name="search-isbn" />
		<xsl:call-template name="search-issn" />
		<xsl:call-template name="search-toc" />
		<xsl:call-template name="search-rsrctype" />
		<xsl:call-template name="search-creationdate" />
		<xsl:call-template name="search-addtitle" />
		<xsl:call-template name="search-searchscope" />
		<xsl:call-template name="search-scope" />
		<xsl:call-template name="search-alttitle" />
		<xsl:call-template name="search-lsr01" /> <!-- Call number search and without spaces-->
	</search>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/creatorcontrib section -->
<xd:doc> <xd:short>Creator / Contributor</xd:short>
  <xd:detail><p>Search parameters for creator/contributor</p>
	<ul>
		<li>100|a when ind1 = 1,2</li>
		<li>100|bcdejqu when ind1 != 1,2</li>
		<li>700|a + bcdejqu when ind1 = 12 and ind2 != 2</li>
		<li>100,700 when ind2 != 2, related 880 |abcdejqu</li>
		<li>100,700|a when ind1 = 1,2 and normalize author name to lastname, first initial</li>
		<li>110,710,810 and related 880 | abcde</li>
		<li>111,711,811 and related 880 | abcdn</li>
		<li>245|c and realted 880</li>
		<li>505|r and related 880</li>
		<li>800| abcdejqu and related 880</li>	
		<li>508,511,720,800|a and related 880</li>
		<li>CR100|CR110|CR111|CR700|CR710|CR711|a-z</li>
	</ul>
	<footer>Last updated: 2013-4-18   By: Eric Nord</footer>
  </xd:detail>
</xd:doc>
<xsl:template name="search-creatorcontrib">
	
	<creatorcontrib>
		<xsl:for-each select="datafield[@tag='100' and matches(@ind1,'[12]')]">
			<xsl:value-of select="lib:get-regular-name(subfield[@code='a'][1])" />
		</xsl:for-each>
		
		<xsl:for-each select="datafield[@tag='100' and matches(@ind1,'[^12]')]">
			<xsl:value-of select="lib:merge(subfield[matches(@code,'[bcdejqu]')])" />
		</xsl:for-each>
	</creatorcontrib>

	<xsl:for-each select="datafield[@tag='700' and matches(@ind1,'[12]') and matches(@ind2,'[^2]')]">
		<creatorcontrib>
			<xsl:value-of select="lib:get-regular-name(subfield[@code='a'][1])" />
			<xsl:text> </xsl:text>
			<xsl:value-of select="lib:merge(subfield[matches(@code,'[bcdejqu]')])" />
		</creatorcontrib>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'100')] | datafield[matches(@tag,'700') and matches(@ind2,'[^2]')]
 						| datafield[matches(@tag,'700') and matches(@ind1,'[^12]') and matches(@ind2,'[^2]')]
 	                    | datafield[@tag='880' and subfield[@code='6' and matches(.,'100|700')]]">
		<creatorcontrib><xsl:value-of select="lib:merge(subfield[matches(@code,'[abcdejqu]')])" /></creatorcontrib>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'100|700') and matches(@ind1,'[12]')]">
		<creatorcontrib><xsl:value-of select="lib:normalize-author(subfield[@code='a'][1])" /></creatorcontrib>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'100|700') and matches(@ind1,'[12]')]">
		<creatorcontrib><xsl:value-of select="subfield[@code='a'][1]" /></creatorcontrib>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'110|710|810')] | datafield[@tag='880' and subfield[@code='6' and matches(.,'110|710|810')]]">
		<creatorcontrib><xsl:value-of select="lib:merge(subfield[matches(@code,'[abcde]')])" /></creatorcontrib>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'111|711|811')] | datafield[@tag='880' and subfield[@code='6' and matches(.,'111|711|811')]]">
		<creatorcontrib><xsl:value-of select="lib:merge(subfield[matches(@code,'[abcdn]')])" /></creatorcontrib>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'245')] | datafield[@tag='880' and subfield[@code='6' and matches(.,'245')]]">
		<creatorcontrib><xsl:value-of select="lib:merge(subfield[matches(@code,'[c]')])" /></creatorcontrib>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'505')] | datafield[@tag='880' and subfield[@code='6' and matches(.,'505')]]">
		<creatorcontrib><xsl:value-of select="lib:merge(subfield[matches(@code,'[r]')])" /></creatorcontrib>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'508|511|720|800')] | datafield[@tag='880' and subfield[@code='6' and matches(.,'508|511|720|800')]]">
		<creatorcontrib><xsl:value-of select="lib:merge(subfield[@code='a'])" /></creatorcontrib>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'800')] | datafield[@tag='880' and subfield[@code='6' and matches(.,'800')]]">
		<creatorcontrib><xsl:value-of select="lib:merge(subfield[matches(@code,'[abcdejqu]')])" /></creatorcontrib>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'CR100|CR110|CR111|CR700|CR710|CR711')]">
		<creatorcontrib><xsl:value-of select="lib:merge(subfield[matches(@code,'[a-z]')])" /></creatorcontrib>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/title section -->
<xd:doc> <xd:short>Search title from display-title. Splits title and subtitle for searching each separately - 130|a and 245|a 245|b</xd:short></xd:doc>
<xsl:template name="search-title">
	<xsl:choose>
		<xsl:when test="lib:validate-fmt('SE') and datafield[@tag='130']">
			<title><xsl:value-of select="datafield[@tag='130']/subfield[matches(@code,'[adfklmnoprs]')]" /></title>
			<title><xsl:value-of select="datafield[@tag='130']/subfield[matches(@code,'[a]')]" /></title>
		</xsl:when>
		<xsl:otherwise>
			<title><xsl:value-of select="replace(lib:merge(datafield[@tag='245']/subfield[matches(@code,'[abfgknp]')]),'[/:;=]$','')" /></title>
			<title><xsl:value-of select="replace(lib:merge(datafield[@tag='245']/subfield[matches(@code,'[a]')]),'[/:;=]$','')" /></title>
			<title><xsl:value-of select="replace(lib:merge(datafield[@tag='245']/subfield[matches(@code,'[b]')]),'[/:;=]$','')" /></title>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/description section -->
<xd:doc> <xd:short>Search description from 520 and related 880</xd:short></xd:doc>
<xsl:template name="search-description">
	
	<xsl:for-each select="datafield[@tag='880' and subfield[@code='6' and matches(.,'520')]]">
		<description><xsl:value-of select="lib:merge(subfield[not(@code='6')])" /></description>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[@tag='520']">
		<description><xsl:value-of select="lib:merge(subfield)" /></description>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/subject section -->
<xd:doc> <xd:short>Search subject</xd:short>
  <xd:detail><p>Search parameters for subject</p>
	<ul>
		<li>600|610|611|630|648|650|651|653|654|655|656|657|658|CR650|CR651|CR600|CR610|CR611|CR630 | a-z and related 880 for 600|610|611|630</li>	
	</ul>
  </xd:detail>
</xd:doc>
<xsl:template name="search-subject">

	<xsl:for-each select="datafield[matches(@tag,'600|610|611|630|648|650|651|653|654|655|656|657|658|CR650|CR651|CR600|CR610|CR611|CR630')]
						| datafield[@tag='880' and subfield[@code='6' and matches(.,'600|610|611|630')]]">
		
		<subject><xsl:value-of select="lib:merge(subfield[matches(@code,'[a-z]')])" /></subject>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/general section -->
<xd:doc> <xd:short>General search</xd:short>
  <xd:detail><p>Additional search fields that didn't fit elsewhere</p>
	<ul>
		<li>260|b and related 880</li>
		<li>502,508,518,521,534,586|alll but '= and 0-9' and related 880</li>
		<li>024 and ind1 = 2,3|az</li>
		<li>027|az</li>
		<li>028|a</li>
		<li>245|h and related 880</li>	
	</ul>
  </xd:detail>
</xd:doc>
<xsl:template name="search-general">

	<xsl:for-each select="datafield[@tag='260'] | datafield[@tag='880' and subfield[@code='6' and matches(., '260')]]">
		<general><xsl:value-of select="lib:merge(subfield[@code='b'])" /></general>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'502|508|518|521|534|586')] | datafield[@tag='880' and subfield[@code='6' and matches(., '502|508|518|521|534|586')]]">
		<general><xsl:value-of select="lib:merge(subfield[matches(@code,'[^=,?0-9]')])" /></general>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'024') and matches(@ind1,'[23]')]">
		<general><xsl:value-of select="lib:merge(subfield[matches(@code,'[az]')])" /></general>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'027')]">
		<general><xsl:value-of select="lib:merge(subfield[matches(@code,'[az]')])" /></general>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[@tag='028']">
		<general><xsl:value-of select="lib:merge(subfield[@code='a'])" /></general>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[@tag='245'] | datafield[@tag='880' and subfield[@code='6' and matches(., '245')]]">
		<general><xsl:value-of select="lib:merge(subfield[@code='h'])" /></general>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/sourceid section -->
<xd:doc> <xd:short>sourceId from control-sourceId</xd:short></xd:doc>
<xsl:template name="search-sourceid">
	<xsl:call-template name="control-sourceid" />
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/recordid section -->
<xd:doc> <xd:short>recordId from control-recordId</xd:short></xd:doc>
<xsl:template name="search-recordid">
		<xsl:call-template name="control-recordid" />
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/isbn section -->
<xd:doc> <xd:short>Search ISBN from 020|az also includes variant with all uppercase and a second variant all uppercase without dashes</xd:short></xd:doc>
<xsl:template name="search-isbn">
	<xsl:for-each select="datafield[@tag='020']/subfield[matches(@code,'[az]')]">
		<!-- first remove everything after the first space, then remove all non-isbn characters -->
		<isbn><xsl:value-of select="replace(replace(upper-case(.),' .*',''),'[^0-9X -]','')" /></isbn>
		<isbn><xsl:value-of select="replace(replace(upper-case(.),' .*',''),'[^0-9X]','')" /></isbn>
	</xsl:for-each>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/issn section -->
<xd:doc> <xd:short>Searhc ISSN from 022|ayz</xd:short></xd:doc>
<xsl:template name="search-issn">
	<xsl:for-each select="datafield[@tag='022']/subfield[matches(@code,'[ayz]')]">
		<issn><xsl:value-of select="." /></issn>
		<issn><xsl:value-of select="replace(.,'-','')" /></issn>
	</xsl:for-each>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/toc section -->
<xd:doc> <xd:short>Table of Contents from 505|a and related 880</xd:short></xd:doc>
<xsl:template name="search-toc">
	<xsl:for-each select="datafield[@tag='505']
						| datafield[@tag='880' and subfield[@code='6' and matches(., '505')]]">
		<toc><xsl:value-of select="lib:merge(subfield[@code='a'])" /></toc>
	</xsl:for-each>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/rsrctype section -->
<xd:doc> <xd:short>Search parameters for resource type from display/type</xd:short></xd:doc>
<xsl:template name="search-rsrctype">
	<xsl:variable name="display-type">
		<xsl:call-template name="display-type" />
	</xsl:variable>
	
	<xsl:for-each select="$display-type/*">
		<rsrctype><xsl:value-of select="." /></rsrctype>
	</xsl:for-each>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/creationdate section -->
<xd:doc> <xd:short>Creation date</xd:short>
  <xd:detail><p>Used for date searching</p>
	<ul>
		<li>call lib:dates</li>	
	</ul>
  </xd:detail>
</xd:doc>
<xsl:template name="search-creationdate">
	<xsl:for-each select="lib:dates(.)">
		<creationdate><xsl:value-of select="." /></creationdate>
	</xsl:for-each>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/addtitle section -->
<xd:doc> <xd:short>Search parameter for add title</xd:short>
  <xd:detail>
	<ul>
		<li>100,110,111|fgklnpt and related 880</li>
		<li>247,440,490,740|abnpv and related 880</li>
		<li>400,410,411|fklnpstv and related 880</li>
		<li>700,710,711,800,810,811|fklmnoprstv and related 880</li>
		<li>730|adfklmnoprs and related 880</li>
		<li>760,762,765,767,770,772,773,774,775,776,777,780,785,786,787|st and related 880</li>
		<li>830,840|760|762|765|767|770|772|773|774|775|776|777|780|785|786|787 and related 880</li>	
	</ul>
  </xd:detail>
</xd:doc>
<xsl:template name="search-addtitle">

	<xsl:for-each select="datafield[matches(@tag,'100|110|111')] | datafield[@tag='880' and subfield[@code='6' and matches(., '100|110|111')]]">
		<addtitle><xsl:value-of select="lib:merge(subfield[matches(@code,'[fgklnpt]')])" /></addtitle>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'247|440|490|740')] | datafield[@tag='880' and subfield[@code='6' and matches(., '247|440|490|740')]]">
		<addtitle><xsl:value-of select="lib:merge(subfield[matches(@code,'[abnpv]')])" /></addtitle>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'400|410|411')] | datafield[@tag='880' and subfield[@code='6' and matches(., '400|410|411')]]">
		<addtitle><xsl:value-of select="lib:merge(subfield[matches(@code,'[fklnpstv]')])" /></addtitle>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'700|710|711|800|810|811')] | datafield[@tag='880' and subfield[@code='6' and matches(., '700|710|711|800|810|811')]]">
		<addtitle><xsl:value-of select="lib:merge(subfield[matches(@code,'[fklmnoprstv]')])" /></addtitle>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'730')] | datafield[@tag='880' and subfield[@code='6' and matches(., '730')]]">">
		<addtitle><xsl:value-of select="lib:merge(subfield[matches(@code,'[adfklmnoprs]')])" /></addtitle>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'760|762|765|767|770|772|773|774|775|776|777|780|785|786|787')] | datafield[@tag='880' and subfield[@code='6' and matches(., '760|762|765|767|770|772|773|774|775|776|777|780|785|786|787')]]">
		<addtitle><xsl:value-of select="lib:merge(subfield[matches(@code,'[st]')])" /></addtitle>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'830|840')] | datafield[@tag='880' and subfield[@code='6' and matches(., '830|840')]]">">
		<addtitle><xsl:value-of select="lib:merge(subfield[matches(@code,'[adfklmnoprstv]')])" /></addtitle>
	</xsl:for-each>
	
</xsl:template>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- contains some generic search scopes used by all institutions -->
<xd:doc> <xd:short>Search scopes provide collections of searches and allow us to search for only one seciton of our records</xd:short>
  <xd:detail>
	<ul>
		<li>Add 'GREEN'</li>
		<li>Add insitution id</li>
		<li>Add sourceId </li>
	</ul>
  </xd:detail>
</xd:doc>
<xsl:template name="search-searchscope-all">
	
	<!-- Scope for institution. ie: BYU -->
	<searchscope><xsl:value-of select="$institution" /></searchscope>
	
	<!-- sourceid. ie:byu -->
	<xsl:variable name="searchscope">
		<xsl:call-template name="control-sourceid" />
	</xsl:variable>
	<xsl:for-each select="$searchscope/*">
		<searchscope><xsl:value-of select="upper-case(.)" /></searchscope>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[@tag='999']/subfield[@code='m']">
		<searchscope><xsl:value-of select="$institution" />_<xsl:value-of select="upper-case(replace(.,'-',''))" /></searchscope>
	</xsl:for-each>
	
	<!-- add a scope for all libraries -->
	<xsl:for-each select="datafield[@tag='999']/subfield[@code='l']">
		<searchscope><xsl:value-of select="$institution" />_<xsl:value-of select="upper-case(.)" /></searchscope>
	</xsl:for-each>

</xsl:template>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- contains some generic search scopes used by all institutions -->
<xd:doc> <xd:short>Generic search scopes</xd:short></xd:doc>
<xsl:template name="search-searchscope">

	<xsl:call-template name="search-searchscope-all" />

</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/scope section -->
<xd:doc> <xd:short>Search scope is a collection of scopes for instance a search scope of BYU-all would include scopes of byu, spec-coll, Primo Central, etc.</xd:short>
  <xd:detail><p>Search scope and scope are currently redundant</p>
	<ul>
		<li>Find scope and apply to search scope</li>
		<li>100|bcdejqu when ind1 != 1,2</li>	
	</ul>
  </xd:detail>
</xd:doc>
<xsl:template name="search-scope">

	<xsl:variable name="searchscope">
		<xsl:call-template name="search-searchscope" />
	</xsl:variable>
	
	<xsl:for-each select="$searchscope/*">
		<scope><xsl:value-of select="." /></scope>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/alttitle section -->
<xd:doc> <xd:short>Search for alt title</xd:short>
  <xd:detail><p>Search parameters for creator/contributor</p>
	<ul>
		<li>130,210,240,243| alll but 0-9 and related 880</li>
		<li>245 related 880|abfgknp</li>
		<li>246|abnp and related 880 </li>	
	</ul>
  </xd:detail>
</xd:doc>
<xsl:template name="search-alttitle">

	<xsl:for-each select="datafield[matches(@tag,'130|210|240|243')]
						| datafield[@tag='880' and subfield[@code='6' and matches(., '130|210|240|243')]]">
		<alttitle><xsl:value-of select="lib:merge(subfield[matches(@code,'[^0-9]')])" /></alttitle>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[@tag='880' and subfield[@code='6' and matches(., '245')]]">
		<alttitle><xsl:value-of select="lib:merge(subfield[matches(@code,'[abfgknp]')])" /></alttitle>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[matches(@tag,'246')]
						| datafield[@tag='880' and subfield[@code='6' and matches(., '246')]]">
		<alttitle><xsl:value-of select="lib:merge(subfield[matches(@code,'[abnp]')])" /></alttitle>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the search/lsr01 section -->
<xd:doc> <xd:short>Call number search from 999|a</xd:short></xd:doc>
<xsl:template name="search-lsr01">

	<xsl:for-each select="datafield[@tag='999']/subfield[@code='a']">
		<lsr01><xsl:value-of select="." /></lsr01>
		<lsr01><xsl:value-of select="replace(.,' ','')" /></lsr01>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


</xsl:stylesheet>
