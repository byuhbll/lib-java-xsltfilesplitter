<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:lib="http://lib.byu.edu/"
	exclude-result-prefixes="lib xd"
	xmlns:saxon="http://icl.com/saxon"
	extension-element-prefixes="saxon">

<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" 
	saxon:character-representation="native;decimal" />

<!-- creates the addata section -->
<xsl:template name="addata">
	<addata>
		<xsl:call-template name="addata-aulast" />
		<xsl:call-template name="addata-aufirst" />
		<xsl:call-template name="addata-au" />
		<xsl:call-template name="addata-aucorp" />
		<xsl:call-template name="addata-addau" />
		<xsl:call-template name="addata-seriesau" />
		<xsl:call-template name="addata-btitle" />
		<xsl:call-template name="addata-jtitle" />
		<xsl:call-template name="addata-stitle" />
		<xsl:call-template name="addata-addtitle" />
		<xsl:call-template name="addata-seriestitle" />
		<xsl:call-template name="addata-date" />
		<xsl:call-template name="addata-volume" />
		<xsl:call-template name="addata-risdate" />
		<xsl:call-template name="addata-issn" />
		<xsl:call-template name="addata-eissn" />
		<xsl:call-template name="addata-isbn" />
		<xsl:call-template name="addata-coden" />
		<xsl:call-template name="addata-format" />
		<xsl:call-template name="addata-genre" />
		<xsl:call-template name="addata-ristype" />
		<xsl:call-template name="addata-notes" />
		<xsl:call-template name="addata-abstract" />
		<xsl:call-template name="addata-cop" />
		<xsl:call-template name="addata-pub" />
		<xsl:call-template name="addata-oclcid" />
		<xsl:call-template name="addata-objectid" />
		<xsl:call-template name="addata-lccn" />
		<xsl:call-template name="addata-url" />
		<xsl:call-template name="addata-lad01" />
		<xsl:call-template name="addata-lad02" />
		<xsl:call-template name="addata-lad03" />
	</addata>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-aulast section -->
<xd:doc>
	<xd:short>Get author last name</xd:short>
	<xd:detail>Get 100|1 or 700|a when ind1 = 1 and apply template "get-last-name"</xd:detail>
</xd:doc>
<xsl:template name="addata-aulast">
	
	<xsl:for-each select="datafield[matches(@tag,'100|700') and matches(@ind1,'[12]')]">
		<!-- Get just the 100 if exists, otherwise get 700 -->
		<xsl:sort select="@tag" />
		<xsl:if test="position()=1">
			<aulast><xsl:value-of select="lib:get-last-name(subfield[@code='a'][1])" /></aulast>
		</xsl:if>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-aufirst section -->
<xd:doc>
	<xd:short>Get author first name</xd:short>
	<xd:detail>Get 100|1 or 700|a when ind1 = 1 and apply template "get-first-name"</xd:detail>
</xd:doc>
<xsl:template name="addata-aufirst">
	
	<xsl:choose>
		<xsl:when test="datafield[@tag='100' and @ind1='1']">
			<xsl:for-each select="datafield[@tag='100' and @ind1='1']">
				<aufirst>
					<xsl:value-of select="lib:get-first-name(subfield[@code='a'][1])" />
				</aufirst>
			</xsl:for-each>
		</xsl:when>
		<xsl:otherwise>
			<xsl:for-each select="datafield[@tag='700' and @ind1='1'][1]">
				<aufirst>
					<xsl:value-of select="lib:get-first-name(subfield[@code='a'][1])" />
				</aufirst>
			</xsl:for-each>
		</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-au section -->
<xd:doc>
	<xd:short>Get all author data</xd:short>
	<xd:detail>Get 100|abcdejqu</xd:detail>
</xd:doc>
<xsl:template name="addata-au">
	
	<au><xsl:value-of select="replace(lib:merge(datafield[@tag='100']/subfield[matches(@code,'[abcdejqu]')]),'[.,/=:]$','')" /></au>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-aucorp section -->
<xd:doc>
	<xd:short>Get author corporation</xd:short>
	<xd:detail>Get 110|abcde, 111|abcdn,</xd:detail>
</xd:doc>
<xsl:template name="addata-aucorp">
	
	<aucorp><xsl:value-of select="replace(lib:merge(datafield[@tag='110']/subfield[matches(@code,'[abcde]')]),'[.,/=:]$','')" /></aucorp>
	<aucorp><xsl:value-of select="replace(lib:merge(datafield[@tag='111']/subfield[matches(@code,'[abcdn]')]),'[.,/=:]$','')" /></aucorp>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-addau section -->
<xd:doc>
	<xd:short>Get author corporation</xd:short>
	<xd:detail>Get 110|abcde, 111|abcdn,</xd:detail>
</xd:doc>

<xsl:template name="addata-addau">
	
	<xsl:for-each select="datafield[@tag='700']"> 
		<addau><xsl:value-of select="replace(lib:merge(subfield[matches(@code,'[abcdejqu]')]),'[.,/=:]$','')" /></addau>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[@tag='710']"> 
		<addau><xsl:value-of select="replace(lib:merge(subfield[matches(@code,'[abcde]')]),'[.,/=:]$','')" /></addau>
	</xsl:for-each>
	
	<xsl:for-each select="datafield[@tag='711']"> 
		<addau><xsl:value-of select="replace(lib:merge(subfield[matches(@code,'[abcdn]')]),'[.,/=:]$','')" /></addau>
	</xsl:for-each>
	
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-seriesau section -->
<xsl:template name="addata-seriesau">

	<xsl:for-each select="datafield[@tag='800']"> 
		<seriesau><xsl:value-of select="replace(lib:merge(subfield[matches(@code,'[abcde]')]),'[.,/=:]$','')" /></seriesau>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-btitle section -->
<xsl:template name="addata-btitle">

	<xsl:variable name="display-type">
		<xsl:call-template name="display-type" />
	</xsl:variable>

	<xsl:if test="not(matches($display-type, 'journal|article'))">
		<btitle>
			<xsl:value-of><xsl:call-template name="display-title" /></xsl:value-of>
		</btitle>
	</xsl:if>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-jtitle section -->
<xsl:template name="addata-jtitle">

	<xsl:variable name="display-type">
		<xsl:call-template name="display-type" />
	</xsl:variable>

	<xsl:if test="matches(lower-case($display-type), 'journal')">
		<jtitle>
			<xsl:value-of><xsl:call-template name="display-title" /></xsl:value-of>
		</jtitle>
	</xsl:if>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-stitle section -->
<xsl:template name="addata-stitle">

	<xsl:for-each select="datafield[@tag='210']"> 
		<stitle><xsl:value-of select="subfield[@code='a']" /></stitle>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-addtitle section -->
<xsl:template name="addata-addtitle">

	<xsl:for-each select="datafield[@tag='246'][1]"> 
		<addtitle><xsl:value-of select="replace(lib:merge(subfield[matches(@code,'[abnp]')]),'[.,/=:]$','')" /></addtitle>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-seriestitle section -->
<xsl:template name="addata-seriestitle">

	<xsl:for-each select="datafield[matches(@tag,'400|410|411|440|800|810|811|830|840')]
						| datafield[@tag='490' and @ind1='0']"> 
		<seriestitle><xsl:value-of select="replace(lib:merge(subfield[matches(@code,'[a-wy-z]')]),'[.,/=:]$','')" /></seriestitle>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-date section -->
<xsl:template name="addata-date">

	<xsl:for-each select="lib:dates(.)">
		<date><xsl:value-of select="."/></date>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<!-- creates the addata-volume section -->
<!-- Requested by: 			Kayla Wiley -->
<!-- Requested date: 		Oct 29th 2012 -->
<!-- Source: 				Scholar Search training meeting-->
<!-- Implementation date: 	Oct 30th 2012 -->
<!-- Implementation by:		Eric Nord -->
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<xsl:template name="addata-volume">

	<volume>
		<xsl:choose>
			<xsl:when test="datafield[matches(@tag,'440')]">
				<xsl:value-of select="datafield[@tag='440']/subfield[matches(@code,'[v]')]" />
			</xsl:when>
			<xsl:when test="datafield[matches(@tag,'830')]">
				<xsl:value-of select="datafield[@tag='830']/subfield[matches(@code,'[v]')]" />
			</xsl:when>
			<xsl:when test="datafield[matches(@tag,'490')]">
				<xsl:value-of select="datafield[@tag='490']/subfield[matches(@code,'[v]')]" />
			</xsl:when>
			<xsl:when test="datafield[matches(@tag,'800')]">
				<xsl:value-of select="datafield[@tag='800']/subfield[matches(@code,'[tv]')]" />
			</xsl:when>
			<xsl:when test="datafield[matches(@tag,'810')]">
				<xsl:value-of select="datafield[@tag='810']/subfield[matches(@code,'[tv]')]" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="datafield[@tag='440']/subfield[matches(@code,'[tv]')]" />
			</xsl:otherwise>
		</xsl:choose>
	</volume>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-risdate section -->
<xsl:template name="addata-risdate">

	<xsl:variable name="risdate" select="lib:merge(lib:dates(.)[matches(@origin,'260-c')]/@original)" />
	
	<risdate>
		<xsl:choose>
			<xsl:when test="not($risdate='')">
				<xsl:value-of select="$risdate" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="lib:dates(.)[matches(@origin,'008-8')]" />
			</xsl:otherwise>
		</xsl:choose>
	</risdate>

</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-issn section -->
<xsl:template name="addata-issn">

	<xsl:for-each select="datafield[@tag='022']/subfield[@code='a']">
		<issn><xsl:value-of select="." /></issn>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-eissn section -->
<xsl:template name="addata-eissn">

	<xsl:for-each select="datafield[@tag='776']/subfield[@code='z']">
		<eissn><xsl:value-of select="replace(.,'(\w+).*','$1')" /></eissn>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-isbn section -->
<xsl:template name="addata-isbn">

	<xsl:for-each select="datafield[@tag='020']/subfield[@code='a']">
		<!-- first remove everything after the first space, then remove all non-isbn characters -->
		<isbn><xsl:value-of select="replace(replace(upper-case(.),' .*',''),'[^0-9X -]','')" /></isbn>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-coden section -->
<xsl:template name="addata-coden">

	<xsl:for-each select="datafield[@tag='030']/subfield[@code='a']">
		<coden><xsl:value-of select="replace(.,'(\w+).*','$1')" /></coden>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-format section -->
<xsl:template name="addata-format">

	<format>
		<xsl:value-of>
			<xsl:call-template name="display-type" />
		</xsl:value-of>
	</format>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-genre section -->
<xsl:template name="addata-genre">

	<genre>
		<xsl:value-of>
			<xsl:call-template name="display-type" />
		</xsl:value-of>
	</genre>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-ristype section -->
<xsl:template name="addata-ristype">

	<xsl:variable name="display-type">
		<xsl:value-of><xsl:call-template name="display-type" /></xsl:value-of>
	</xsl:variable>
	
	<ristype><xsl:value-of select="lib:get-from-map($risType,$display-type)" /></ristype>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-notes section -->
<xsl:template name="addata-notes">

	<notes>
		<xsl:value-of select="datafield[matches(@tag,'502|504')]/subfield[@code='a']" separator=" ; "/>
	</notes>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-abstract section -->
<xsl:template name="addata-abstract">

	<xsl:for-each select="datafield[@tag='520']">
		<abstract><xsl:value-of select="subfield[matches(@code,'[ab]')]" /></abstract>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-cop section -->
<xsl:template name="addata-cop">

	<xsl:for-each select="datafield[@tag='260']">
		<cop><xsl:value-of select="replace(lib:merge(subfield[@code='a']), '^\[|[.,:/=\]]$', '')" /></cop>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-pub section -->
<xsl:template name="addata-pub">

	<xsl:for-each select="datafield[@tag='260']">
		<pub><xsl:value-of select="replace(lib:merge(subfield[@code='b']),'[.,:/=]$','')" /></pub>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-oclcid section -->
<xsl:template name="addata-oclcid">

	<xsl:for-each select="datafield[@tag='035']/subfield[@code='a' and matches(.,'^\(OCoLC\)')]">
		<oclcid><xsl:value-of select="replace(.,'^\(OCoLC\)[\D]*','')" /></oclcid>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-objectid section -->
<xsl:template name="addata-objectid">

	<xsl:for-each select="datafield[@tag='090']/subfield[@code='a']">
		<objectid><xsl:value-of select="." /></objectid>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- creates the addata-lccn section -->
<xsl:template name="addata-lccn">

	<xsl:for-each select="datafield[@tag='010']/subfield[@code='a']">
		<lccn><xsl:value-of select="replace(.,'^\s*([a-z]+\s\S+|\S+).*$','$1')" /></lccn>
	</xsl:for-each>
	
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- Films On Demand - Embedded video link -->
<!-- Added to provide a link to the Front End (red) for embedded video thumbnail supplied from Films On Demand EN March 2 2011 -->
<xsl:template name="addata-url">
	<xsl:if test="controlfield[@tag='003' and starts-with(., 'FOD')]">
		<xsl:for-each select="datafield[@tag='856']/subfield[@code='u']">
			<url>
				<xsl:text>https://lib.byu.edu/cgi-bin/remoteauth.pl?url=http://digital.films.com/OnDemandEmbed.aspx?Token=</xsl:text>
				<xsl:value-of select="replace(., '.*xtid=(\d+).*', '$1')" />
				<xsl:text>&amp;aid=</xsl:text>
				<xsl:value-of select="replace(., '.*aid=(\d+).*', '$1')" />
				<xsl:text>&amp;Plt=FOD&amp;loid=0&amp;w=320&amp;h=240</xsl:text>
			</url>
		</xsl:for-each>
	</xsl:if>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- insert the boundwith child cat id -->
<xsl:template name="addata-lad01">
	<xsl:for-each select="datafield[@tag='999' and subfield[@code='a'] and subfield[@code='z']]">
		<lad01>
			<xsl:text>$$A</xsl:text>
			<xsl:value-of select="./subfield[@code='a']" />
			<xsl:text>$$Z</xsl:text>
			<xsl:value-of select="./subfield[@code='z']" />
		</lad01>
	</xsl:for-each>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- insert the boundwith parent cat id -->
<xsl:template name="addata-lad02">
	<xsl:for-each select="datafield[@tag='992' and subfield[@code='a'] and subfield[@code='z']]">
		<lad02>
			<xsl:text>$$A</xsl:text>
			<xsl:value-of select="./subfield[@code='a']" />
			<xsl:text>$$Z</xsl:text>
			<xsl:value-of select="./subfield[@code='z']" />
		</lad02>
	</xsl:for-each>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!-- used for the IMDb ID (tt0241527) -->
<xsl:template name="addata-lad03">
	<xsl:for-each select="datafield[@tag='035']/subfield[@code='a' and matches(., '\(IMDb\)')]">
		<lad03><xsl:value-of select="replace(., '\(IMDb\)', '')"/></lad03>
	</xsl:for-each>
</xsl:template>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


</xsl:stylesheet>
