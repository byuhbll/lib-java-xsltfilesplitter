<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xd="http://www.pnp-software.com/XSLTdoc"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:lib="http://lib.byu.edu/"
	xmlns:map="urn:schemas-microsoft-com:office:spreadsheet"
	exclude-result-prefixes="lib map xs xd"
	xmlns:saxon="http://icl.com/saxon"
	extension-element-prefixes="saxon">
	

<xsl:variable name="root" select="/" />
<xsl:variable name="fmt" select="lib:get-fmt(//leader)" />

<xsl:variable name="apos">'</xsl:variable>
<xsl:variable name="quot">"</xsl:variable>

<xd:doc>Replaces characters in a string</xd:doc>
<xsl:function name="lib:format-year">
	<xsl:param name="string" />
	<xsl:param name="replace" />
	
	<xsl:value-of select="replace(lib:trim(substring($string, 1, 4)), '\D', $replace)" />
	
</xsl:function>


<xd:doc>Removes characters from the end of a string</xd:doc>
<xsl:function name="lib:remove-characters-from-end">
	<xsl:param name="string" />
	<xsl:param name="chars" />
	
	<xsl:variable name="pattern" select="concat('[', $chars, ']+$')" />
	<xsl:value-of select="lib:trim(replace($string, $pattern, ''))" />
</xsl:function>

<xd:doc>Format year: Gets the first four characters of the given string and replaces all characters that are not digits with the specified parameter.</xd:doc>
<xsl:function name="lib:format-year">
	<xsl:param name="year" />
	
	
	<xsl:variable name="year" select="replace($year,'^\[?(\w{4}).*','$1')" />
	<xsl:variable name="year" select="replace($year,'\D','0')" />
	<xsl:value-of select="$year" />
</xsl:function>

<xd:doc>Returns a new list of fields with their text values having gone through the replace</xd:doc>
<!-- 
Returns a new list of fields with their text values having gone through the replace 
@Definition: lib:fields-replace(fields, pattern, replace)
@Param: The set of nodes over which to iterate (<subfield>...</subfield><subfield>...</subfield>)
@Param: Regular expression pattern for searching (';')
@Param: The replace string ('')
@Example: lib:fields-replace(<subfield>...</subfield><subfield>...</subfield>, ';', '')
-->
<xsl:function name="lib:fields-replace">
	<xsl:param name="fields" />
	<xsl:param name="pattern" />
	<xsl:param name="replace" />
	
	<xsl:for-each select="$fields">
		<xsl:copy>
			<xsl:value-of select="replace(., $pattern, $replace)" />		
		</xsl:copy>
	</xsl:for-each>
</xsl:function>

<xd:doc>Deduplicates a list of fields according to the field's text value</xd:doc>
<!-- 
Deduplicates a list of fields according to the field's text value
@Definition: lib:dedup(fields)
@Param: (fields) The set of nodes to dedup
@Example: lib:dedup(<lang>eng</lang><lang>eng</lang><lang>spa</lang>)
@Return: <lang>eng</lang><lang>spa</lang>
-->
<xsl:function name="lib:dedup">
	<xsl:param name="fields" />
	
	<xsl:for-each select="$fields">
		<xsl:if test="not(preceding-sibling::*[current()=.])">
			<xsl:copy-of select="." />
		</xsl:if>
	</xsl:for-each>
</xsl:function>

<xd:doc>Trim leading and trailing whitespace</xd:doc>
<!-- trim leading and trailing whitespace -->
<xsl:function name="lib:trim">
	<xsl:param name="string" />
	<xsl:value-of select="replace(replace($string, '^\s*(.+?)\s*$', '$1'), '^\s+$', '')" />
</xsl:function>


<xd:doc>Get the format code as described in primodocumentation</xd:doc>
<xsl:function name="lib:get-fmt">
	<xsl:param name="leader-node" />
	<xsl:variable name="leader" select="substring($leader-node, 7, 2)" />
	<xsl:variable name="leader6" select="substring($leader-node, 7, 1)" />
	
	<xsl:choose>
		<xsl:when test="matches($leader, 'a[acdm]')">BK</xsl:when>
		<xsl:when test="matches($leader, 'a[bis]')">SE</xsl:when>
		<xsl:when test="$leader6 = 'c'">SC</xsl:when>
		<xsl:when test="$leader6 = 'd'">SC</xsl:when>
		<xsl:when test="$leader6 = 'e'">MP</xsl:when>
		<xsl:when test="$leader6 = 'f'">MP</xsl:when>
		<xsl:when test="$leader6 = 'g'">VM</xsl:when>
		<xsl:when test="$leader6 = 'i'">MU</xsl:when>
		<xsl:when test="$leader6 = 'j'">MU</xsl:when>
		<xsl:when test="$leader6 = 'k'">VM</xsl:when>
		<xsl:when test="$leader6 = 'm'">CF</xsl:when>
		<xsl:when test="$leader6 = 'o'">VM</xsl:when>
		<xsl:when test="$leader6 = 'p'">MX</xsl:when>
		<xsl:when test="$leader6 = 'r'">VM</xsl:when>
		<xsl:when test="$leader6 = 't'">BK</xsl:when>
		<xsl:otherwise>OT</xsl:otherwise>
	</xsl:choose>
</xsl:function>


<xd:doc>Returns boolean if arg equals the format of the document</xd:doc>
<xsl:function name="lib:validate-fmt">
	<xsl:param name="fmt-to-check" />
	<xsl:sequence select="$fmt-to-check=$fmt" />
</xsl:function>

<xd:doc>See template->lib:merge(fields,delimiter). Example: lib:merge(subfield[matches(@code,'[abc]')]) Returns "a b c"</xd:doc>
<!-- 
@See template->lib:merge(fields,delimiter)
@Example: lib:merge(subfield[matches(@code,'[abc]')])
@Returns: <field>a b c</field>
-->
<xsl:function name="lib:merge">
	<xsl:param name="fields" />
	
	<xsl:call-template name="lib:merge">
		<xsl:with-param name="fields" select="$fields" />
	</xsl:call-template>
</xsl:function>

<xd:doc>See template->lib:merge(fields,delimiter). Example: lib:merge(subfield[matches(@code,'[abc]')]) Returns "a; b; c"</xd:doc>
<!-- 
@See template->lib:merge(fields,delimiter)
@Example: lib:merge(subfield[matches(@code,'[abc]')],'; ')
@Returns: <field>a; b; c</field>
-->
<xsl:function name="lib:merge">
	<xsl:param name="fields" />
	<xsl:param name="delimiter" />
	
	<xsl:call-template name="lib:merge">
		<xsl:with-param name="fields" select="$fields" />
		<xsl:with-param name="delimiter" select="$delimiter" />
	</xsl:call-template>
</xsl:function>

<xd:doc>Concatenates the value of each node in $fields using the specified delimiter and returns the resulting value inside a field element</xd:doc>
<!-- 
Concatenates the value of each node in $fields using the specified delimiter and returns the resulting value inside a field element 
@Definition: lib:merge-fields(fields) or lib:merge-fields(fields, delimiter)
@Param: (fields) The set of nodes over which to iterate (<subfield>...</subfield><subfield>...</subfield>)
@Param: (delimiter, @default=' ') Delimiter for merging the values (;)
@Example: 
	<xsl:call-template name="lib:merge">
		<xsl:with-param name="fields">
			<xsl:for-each select="subfield[matches(@code,'[abc]')]">
				<xsl:copy-of select="." />
			</xsl:for-each>
		</xsl:with-param>
		<xsl:with-param name="delimiter" select="';'" />
	</xsl:call-template>
@Returns: <field>a; b; c</field>
-->
<xsl:template name="lib:merge">
	<xsl:param name="fields" />
	<xsl:param name="delimiter" select="' '" />
	
	<xsl:variable name="string">
		<xsl:for-each select="$fields[.!='']">
			<xsl:value-of select="." />
			
			<xsl:if test="position() != last()">
				<xsl:value-of select="$delimiter" />
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>
	
	<xsl:if test="$string">
		<field>
			<xsl:value-of select="lib:trim($string)" />
		</field>
	</xsl:if>
</xsl:template>

<xd:doc>SameAs: lib:get-from-map($map, $key, 2, 3)</xd:doc>
<!-- 
@SameAs: lib:get-from-map($map, $key, 2, 3)
-->
<xsl:function name="lib:get-from-map">
	<xsl:param name="map" />
	<xsl:param name="key" />
	
	<xsl:value-of select="lib:get-from-map($map, $key, 2, 3)" />
</xsl:function>

<xd:doc>Look up a value in a mapping table. Format Excel 2003 XML.</xd:doc>
<!-- 
Look up a value in a mapping table. Format Excel 2003 XML. 
@Definition: lib:get-from-map(map, key, keyCol, valueCol)
@Param: The map read in from marc.xsl ($ils_lib_codes)
@Param: The key to look up in the second column (INTERNET)
@Param: The key column (2)
@Param: The value column (3)
@Example: lib:get-from-map($ils_lib_codes, 'INTERNET', 2, 3)
@Result: INTERNET
-->
<xsl:function name="lib:get-from-map">
	<xsl:param name="map" />
	<xsl:param name="key" />
	<xsl:param name="keyCol" />
	<xsl:param name="valueCol" />
	
	<xsl:variable name="value" select="$map/map:Workbook/map:Worksheet[1]/map:Table/map:Row[map:Cell[$keyCol]/map:Data[.=$key]][1]/map:Cell[$valueCol]/map:Data" />
	<xsl:value-of select="$value" />
</xsl:function>

<xd:doc>Look up a value in a mapping table. Format Excel 2003 XML. Key is in the second column and the value in the third</xd:doc>
<!-- 
Look up a value in a mapping table. Format Excel 2003 XML. Key is in the second column and the value in the third. 
@Definition: lib:get-from-map(map, key)
@Param: The map read in from marc.xsl ($ils_lib_codes)
@Param: The key to look up in the second column (INTERNET)
@Example: lib:get-from-map($ils_lib_codes, 'INTERNET')
@Result: INTERNET
-->
<xsl:function name="lib:get-notes-from-map">
	<xsl:param name="tag" />
	<xsl:param name="ind1" />
	
	<xsl:variable name="value" select="$symphonyNotes/descendant::map:Row[map:Cell[1]/map:Data[.=$tag] and map:Cell[2]/map:Data[.=$ind1 or .='*']][1]/map:Cell[3]/map:Data" />
	<xsl:value-of select="$value" />
</xsl:function>

<xd:doc>Splits string at multiple and returns a nodelist of matches</xd:doc>
<!-- 
Splits string at multiple and returns a nodelist of matches (<split>eng</split><split>chi</split>) 
@Definition: lib:split-interval(string, interval)
@Param: The string to split (engchi)
@Param: Multiple or interval for splitting (3)
@Example: lib:split-interval('engchi', '3')
@Result: <split>eng</split><split>chi</split>
-->
<xsl:function name="lib:split-interval">
	<xsl:param name="string" />
	<xsl:param name="interval"/>
	
	<xsl:variable name="length" select="ceiling((string-length($string) div $interval)) cast as xs:integer" />
	
	<xsl:for-each select="1 to $length">
		<split><xsl:value-of select="substring($string, ((. - 1) * $interval + 1 ), $interval)" /></split>
	</xsl:for-each>
</xsl:function>

<xd:doc>Splits string at pattern and returns a nodelist of matches</xd:doc>
<!-- 
Splits string at pattern and returns a nodelist of matches (<split>eng</split><split>chi</split>) 
@Definition: lib:split(string, pattern)
@Param: The string to split (eng;chi)
@Param: Pattern as delimiter (;)
@Example: lib:split('eng;chi', ';')
@Result: <split>eng</split><split>chi</split>
-->
<xsl:function name="lib:split">
	<xsl:param name="string" />
	<xsl:param name="pattern"/>

	<xsl:for-each select="tokenize($string, $pattern)">
		<field><xsl:value-of select="." /></field>
	</xsl:for-each>
	
</xsl:function>

<xd:doc>Returns the author's lastname and first initial</xd:doc>
<!-- 
Returns the author's lastname and first initial 
@Definition: lib:normalize-author(string)
@Param: The author's name with lastname first (Bargebuhr, Frederick P.,)
@Example: lib:field-exists('Bargebuhr, Frederick P.,')
@Result: Bargebuhr, F
-->
<xsl:function name="lib:normalize-author">
	<xsl:param name="author" />
	
	<xsl:value-of select="replace(lib:get-name($author), '(.*,\s*.).*', '$1')" />
</xsl:function>


<xd:doc>Returns the author's first names or blank if there is only a last name and removes period</xd:doc>
<!-- 
Returns the author's first names or blank if there is only a last name. Removes period
at the end if the first name is not an initial
@Definition: lib:get-first-name(value)
@Param: (value) The author's name with lastname first (Bargebuhr, Frederick P.)
@Example: lib:get-first-name('Bargebuhr, Frederick P.')
@Result: Frederick P.
-->
<xsl:function name="lib:get-first-name">
        <xsl:param name="value" />

        <xsl:if test="matches($value, ',')">
                <xsl:value-of select="normalize-space(replace(replace($value, '^.*?,(.*?),?$', '$1'), '(\w\w)\.$', '$1'))" />
        </xsl:if>
</xsl:function>


<xd:doc> 
Returns the author's last name 
@Definition: lib:get-last-name(string)
@Param: The author's name with lastname first (Bargebuhr, Frederick P.,)
@Example: lib:get-last-name('Bargebuhr, Frederick P.,')
@Result: Bargebuhr
</xd:doc>
<xsl:function name="lib:get-last-name">
	<xsl:param name="string" />
	
	<xsl:value-of select="lib:trim(replace($string, '(.*?),.*', '$1'))" />
</xsl:function>


<xd:doc>
Returns the individual's full name with other stuff removed. Removes period
at the end if the first name is not an initial
@Definition: lib:get-name(value)
@Param: (value) The author's name with lastname first (Bargebuhr, Frederick P.)
@Example: lib:get-name('Bargebuhr, Frederick P.,')
@Result: Bargebuhr, Frederick P.
</xd:doc>
<xsl:function name="lib:get-name">
	<xsl:param name="value" />
	
	<xsl:value-of select="normalize-space(replace(replace($value, ',$', ''), '(\w\w)\.$', '$1'))" />
</xsl:function>



<xd:doc>
Returns the individual's full name with first name first.
@Definition: lib:get-regular-name(value)
@Param: (value) The author's name with lastname first (Bargebuhr, Frederick P.)
@Example: lib:get-regular-name('Bargebuhr, Frederick P.,')
@Result: Frederick P. Bargebuhr
</xd:doc>
<xsl:function name="lib:get-regular-name">
	<xsl:param name="value" />
	
	<xsl:value-of select="normalize-space(concat(lib:get-first-name($value), ' ', lib:get-last-name($value)))" />
</xsl:function>


<xd:doc>Returns the first word - everything before the first space or the whole value if there is no space</xd:doc>
<!-- 
Returns the first word - everything before the first space or the whole value if there is no space 
@Definition: lib:first-word(nodeset)
@Param: The nodeset (<subfield>9578626088 ( pbk. )</subfield>)
@Example: lib:first-word(<subfield>9578626088 ( pbk. )</subfield>)
@Result: <subfield>9578626088</subfield>
-->
<xsl:function name="lib:first-word">
	<xsl:param name="fields"/>
	
	<xsl:for-each select="$fields">
		<xsl:copy>
			<xsl:copy-of select="@*" />
			<xsl:value-of select="replace(., '(.*?) .*', '$1')" />
		</xsl:copy>
	</xsl:for-each>
</xsl:function>

<xd:doc>Wraps a text value into a field element for use with other functions</xd:doc>
<!-- 
Wraps a text value into a field element for use with other functions
@Definition: lib:wrap(value)
@Param: (value) The string value to be wrapped
@Example: lib:wrap('Gabbert, Laura')
@Result: <field>Gabbert, Laura</field>
-->
<xsl:function name="lib:wrap">
	<xsl:param name="value"/>
	
	<field><xsl:value-of select="$value" /></field>
</xsl:function>


<xd:doc>
Cleans up a resulting pnx record
1- Trims and eliminates redundant spaces in values
2- Removes duplicate fields 
3- Removes blank fields 
It's much easier to do this at the very end than to worry about it in every template 
</xd:doc>
<!-- 
Cleans up a resulting pnx record.
1. Trims and eliminates redundant spaces in values.
2. Removes duplicate fields
3. Removes blank fields
It's much easier to do this at the very end than to worry about it in every template 
@Definition: lib:normalize-pnx(record)
@Param: (record) The resulting pnx record
@Example: lib:normalize-pnx($record) 
@Returns: <record><display>...</display>...</record>
-->
<xsl:function name="lib:normalize-pnx">
	<xsl:param name="record" />
	
	<!-- Clean text -->
	<xsl:variable name="record">
		<record>
			<xsl:for-each select="$record/*/*">
				<xsl:copy>
					<xsl:for-each select="*">
						<xsl:copy>
							<xsl:value-of select="normalize-space()" />
						</xsl:copy>
					</xsl:for-each>
				</xsl:copy>
			</xsl:for-each>
		</record>
	</xsl:variable>
	
	
	<!-- Remove duplicates and empty fields -->
	<record>
		<xsl:for-each select="$record/*/*">
			<xsl:copy>
				<xsl:for-each select="*">
					<!-- remove duplicate fields -->
					<!-- test if this field has any preceding siblings with the same name and same text -->
					<xsl:if test="not(preceding-sibling::*[name()=name(current())][.=current()])">
						<!-- copy if not empty -->
						<xsl:if test="child::node()">
							<xsl:copy>
								<xsl:value-of select="lib:trim(text())" />
							</xsl:copy>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
			</xsl:copy>
		</xsl:for-each>
	</record>
</xsl:function>

<xd:doc>Returns a list of date elements with all dates (years) found order by most desirable date</xd:doc>
<!-- 
Returns a list of date elements with all dates (years) found order by most desirable date
@Definition: lib:dates()
@Example: lib:dates()
@Result: <date>2011</date><date>2010</date><date>2011</date>
-->
<xsl:function name="lib:dates">
	<xsl:param name="record" />
	
	<!-- get dates -->
	<xsl:variable name="dates">
		<xsl:for-each select="$record/controlfield[@tag='008']">
			<date origin="008-8"><xsl:value-of select="substring(., 8, 4)" /></date>
		</xsl:for-each>
		
		<xsl:for-each select="$record/datafield[@tag='260']/subfield[@code='c']">
			<date origin="260-c"><xsl:value-of select="." /></date>
		</xsl:for-each>
		
		<xsl:for-each select="$record/controlfield[@tag='008']">
			<date origin="008-12"><xsl:value-of select="substring(., 12, 4)" /></date>
		</xsl:for-each>
	</xsl:variable>
	
	<!-- clean dates -->
	<xsl:variable name="dates">
		<xsl:for-each select="$dates/*">
			<xsl:if test="text()">
				<xsl:copy>
					<xsl:copy-of select="@*" />
					<xsl:attribute name="original" select="." />
					<xsl:value-of select="substring(concat(replace(., '\D', ''), '0000'), 1, 4)" />
				</xsl:copy>
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>
	
	<!-- verify dates -->
	<xsl:for-each select="$dates/*">
		<xsl:copy>
			<xsl:copy-of select="@*" />
			<xsl:if test="not(starts-with(., '0') or . = '9999')">
				<xsl:copy-of select="." />
			</xsl:if>
		</xsl:copy>
	</xsl:for-each>
	
</xsl:function>

<xd:doc>Returns the prepared key for frbr section</xd:doc>
<!-- 
Returns the prepared key for frbr section
@Definition: lib:prepFrbrKey(key)
@Param: The key
@Example: lib:prepFrbrKey(key)
@Result: <subfield>9578626088</subfield>
-->
<xsl:function name="lib:prepFrbrKey">
	<xsl:param name="key"/>
	
	<xsl:variable name="result" select="lower-case($key)" />
	<xsl:variable name="result" select="replace($result, '[\|\[\]‘,\$~’\^%\*/\\\?@\.:;&lt;&gt;\{\}\-\(\)”;!¿¡]', ' ')" />
	<xsl:variable name="result" select="lib:trim($result)" />
	<xsl:variable name="result" select="lib:nacoDiacriticConverstion($result)" />

	<xsl:value-of select="$result" />
</xsl:function>

<xd:doc>Returns the prepared key for frbr section (advanced)</xd:doc>
<!-- 
Returns the prepared key for frbr section
@Definition: lib:prepFrbrKeyAdvanced(key)
@Param: The key
@Example: lib:prepFrbrKeyAdvanced(key)
@Result: <subfield>9578626088</subfield>
-->
<xsl:function name="lib:prepFrbrKeyAdvanced">
	<xsl:param name="key"/>
	<xsl:param name="ind"/>
	
	<xsl:variable name="ind">
		<xsl:choose>
			<xsl:when test="matches($ind, '^\d+$')"><xsl:value-of select="$ind" /></xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="result" select="substring($key, $ind + 1)" />
	<xsl:variable name="result" select="replace($result, '&lt;&lt;(.*)&gt;&gt;|&#x0088;(.*)&#x0089;', '$1')" />
	<xsl:variable name="result" select="lib:prepFrbrKey($result)" />
	
	<xsl:value-of select="$result" />
</xsl:function>

<xd:doc>Returns diacritic values</xd:doc>
<!-- 
Returns 
@Definition: lib:nacoDiacriticConverstion(value)
@Param: The key
@Example: lib:nacoDiacriticConverstion(value)
@Result: 
-->
<xsl:function name="lib:nacoDiacriticConverstion">
	<xsl:param name="value"/>
	<xsl:variable name="result" select="$value" />

	<xsl:variable name="result" select="replace($result, '&#x012A;', '&#x0049;')" />
	<xsl:variable name="result" select="replace($result, '&#x012B;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x012C;', '&#x0049;')" />
	<xsl:variable name="result" select="replace($result, '&#x012D;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x012E;', '&#x0049;')" />
	<xsl:variable name="result" select="replace($result, '&#x012F;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x0130;', '&#x0049;')" />
	<xsl:variable name="result" select="replace($result, '&#x0131;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x0132;', '&#x0049;-&#x004A;')" />
	<xsl:variable name="result" select="replace($result, '&#x0133;', '&#x0069;-&#x006A;')" />
	<xsl:variable name="result" select="replace($result, '&#x0134;', '&#x004A;')" />
	<xsl:variable name="result" select="replace($result, '&#x0135;', '&#x006A;')" />
	<xsl:variable name="result" select="replace($result, '&#x0136;', '&#x004B;')" />
	<xsl:variable name="result" select="replace($result, '&#x0137;', '&#x006B;')" />
	<xsl:variable name="result" select="replace($result, '&#x0138;', '&#x004B;')" />
	<xsl:variable name="result" select="replace($result, '&#x0139;', '&#x006C;')" />
	<xsl:variable name="result" select="replace($result, '&#x013A;', '&#x004C;')" />
	<xsl:variable name="result" select="replace($result, '&#x013B;', '&#x006C;')" />
	<xsl:variable name="result" select="replace($result, '&#x013C;', '&#x004C;')" />
	<xsl:variable name="result" select="replace($result, '&#x013D;', '&#x006C;')" />
	<xsl:variable name="result" select="replace($result, '&#x013E;', '&#x004C;')" />
	<xsl:variable name="result" select="replace($result, '&#x0192;', '&#x0066;')" />
	<xsl:variable name="result" select="replace($result, '&#x0193;', '&#x0047;')" />
	<xsl:variable name="result" select="replace($result, '&#x0194;', '&#x0047;')" />
	<xsl:variable name="result" select="replace($result, '&#x0197;', '&#x0049;')" />
	<xsl:variable name="result" select="replace($result, '&#x0198;', '&#x004B;')" />
	<xsl:variable name="result" select="replace($result, '&#x0199;', '&#x006B;')" />
	<xsl:variable name="result" select="replace($result, '&#x019A;', '&#x004C;')" />
	<xsl:variable name="result" select="replace($result, '&#x019B;', '&#x006C;')" />
	<xsl:variable name="result" select="replace($result, '&#x019D;', '&#x004E;')" />
	<xsl:variable name="result" select="replace($result, '&#x019E;', '&#x006E;')" />
	<xsl:variable name="result" select="replace($result, '&#x019F;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x01A0;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x01A1;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x01A2;', '&#x004F;-&#x0049;')" />
	<xsl:variable name="result" select="replace($result, '&#x01A3;', '&#x006F;-&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x01A4;', '&#x0050;')" />
	<xsl:variable name="result" select="replace($result, '&#x01A5;', '&#x0070;')" />
	<xsl:variable name="result" select="replace($result, '&#x01A6;', '&#x0052;')" />
	<xsl:variable name="result" select="replace($result, '&#x01A7;', '&#x0053;')" />
	<xsl:variable name="result" select="replace($result, '&#x01A8;', '&#x0073;')" />
	<xsl:variable name="result" select="replace($result, '&#x01A9;', '&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x01AA;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x01AB;', '&#x0074;')" />
	<xsl:variable name="result" select="replace($result, '&#x01AC;', '&#x0054;')" />
	<xsl:variable name="result" select="replace($result, '&#x01AD;', '&#x0074;')" />
	<xsl:variable name="result" select="replace($result, '&#x010D;', '&#x0063;')" />
	<xsl:variable name="result" select="replace($result, '&#x010E;', '&#x0044;')" />
	<xsl:variable name="result" select="replace($result, '&#x010F;', '&#x0064;')" />
	<xsl:variable name="result" select="replace($result, '&#x0110;', '&#x0044;')" />
	<xsl:variable name="result" select="replace($result, '&#x0111;', '&#x0064;')" />
	<xsl:variable name="result" select="replace($result, '&#x0112;', '&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x0113;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x0114;', '&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x0115;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x0116;', '&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x0117;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x0118;', '&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x013F;', '&#x006C;')" />
	<xsl:variable name="result" select="replace($result, '&#x0140;', '&#x004C;')" />
	<xsl:variable name="result" select="replace($result, '&#x0141;', '&#x006C;')" />
	<xsl:variable name="result" select="replace($result, '&#x0142;', '&#x004C;')" />
	<xsl:variable name="result" select="replace($result, '&#x0143;', '&#x006E;')" />
	<xsl:variable name="result" select="replace($result, '&#x0144;', '&#x004E;')" />
	<xsl:variable name="result" select="replace($result, '&#x0145;', '&#x006E;')" />
	<xsl:variable name="result" select="replace($result, '&#x0146;', '&#x004E;')" />
	<xsl:variable name="result" select="replace($result, '&#x0147;', '&#x006E;')" />
	<xsl:variable name="result" select="replace($result, '&#x0148;', '&#x004E;')" />
	<xsl:variable name="result" select="replace($result, '&#x0149;', '&#x006E;')" />
	<xsl:variable name="result" select="replace($result, '&#x014A;', '&#x004E;')" />
	<xsl:variable name="result" select="replace($result, '&#x014B;', '&#x006E;')" />
	<xsl:variable name="result" select="replace($result, '&#x014C;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x014D;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x014E;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x014F;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x0150;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x0151;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x0152;', '&#x004F;-&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x0153;', '&#x006F;-&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x0154;', '&#x0052;')" />
	<xsl:variable name="result" select="replace($result, '&#x0155;', '&#x0072;')" />
	<xsl:variable name="result" select="replace($result, '&#x0156;', '&#x0052;')" />
	<xsl:variable name="result" select="replace($result, '&#x0157;', '&#x0072;')" />
	<xsl:variable name="result" select="replace($result, '&#x0158;', '&#x0052;')" />
	<xsl:variable name="result" select="replace($result, '&#x0159;', '&#x0072;')" />
	<xsl:variable name="result" select="replace($result, '&#x015A;', '&#x0053;')" />
	<xsl:variable name="result" select="replace($result, '&#x015B;', '&#x0073;')" />
	<xsl:variable name="result" select="replace($result, '&#x015C;', '&#x0053;')" />
	<xsl:variable name="result" select="replace($result, '&#x015D;', '&#x0073;')" />
	<xsl:variable name="result" select="replace($result, '&#x015E;', '&#x0053;')" />
	<xsl:variable name="result" select="replace($result, '&#x015F;', '&#x0073;')" />
	<xsl:variable name="result" select="replace($result, '&#x0160;', '&#x0053;')" />
	<xsl:variable name="result" select="replace($result, '&#x0161;', '&#x0073;')" />
	<xsl:variable name="result" select="replace($result, '&#x0162;', '&#x0054;')" />
	<xsl:variable name="result" select="replace($result, '&#x0163;', '&#x0074;')" />
	<xsl:variable name="result" select="replace($result, '&#x0164;', '&#x0054;')" />
	<xsl:variable name="result" select="replace($result, '&#x0165;', '&#x0074;')" />
	<xsl:variable name="result" select="replace($result, '&#x0166;', '&#x0054;')" />
	<xsl:variable name="result" select="replace($result, '&#x0167;', '&#x0074;')" />
	<xsl:variable name="result" select="replace($result, '&#x0168;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x0169;', '&#x0075;')" />
	<xsl:variable name="result" select="replace($result, '&#x016A;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x016B;', '&#x0075;')" />
	<xsl:variable name="result" select="replace($result, '&#x016C;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x016D;', '&#x0075;')" />
	<xsl:variable name="result" select="replace($result, '&#x016E;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x016F;', '&#x0075;')" />
	<xsl:variable name="result" select="replace($result, '&#x0170;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x0171;', '&#x0075;')" />
	<xsl:variable name="result" select="replace($result, '&#x0172;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x0173;', '&#x0075;')" />
	<xsl:variable name="result" select="replace($result, '&#x0174;', '&#x0057;')" />
	<xsl:variable name="result" select="replace($result, '&#x0175;', '&#x0077;')" />
	<xsl:variable name="result" select="replace($result, '&#x0176;', '&#x0059;')" />
	<xsl:variable name="result" select="replace($result, '&#x0177;', '&#x0079;')" />
	<xsl:variable name="result" select="replace($result, '&#x0178;', '&#x0059;')" />
	<xsl:variable name="result" select="replace($result, '&#x0179;', '&#x005A;')" />
	<xsl:variable name="result" select="replace($result, '&#x017A;', '&#x007A;')" />
	<xsl:variable name="result" select="replace($result, '&#x017B;', '&#x005A;')" />
	<xsl:variable name="result" select="replace($result, '&#x017C;', '&#x007A;')" />
	<xsl:variable name="result" select="replace($result, '&#x017D;', '&#x005A;')" />
	<xsl:variable name="result" select="replace($result, '&#x017E;', '&#x007A;')" />
	<xsl:variable name="result" select="replace($result, '&#x017F;', '&#x0073;')" />
	<xsl:variable name="result" select="replace($result, '&#x0180;', '&#x0062;')" />
	<xsl:variable name="result" select="replace($result, '&#x0181;', '&#x0042;')" />
	<xsl:variable name="result" select="replace($result, '&#x0182;', '&#x0062;')" />
	<xsl:variable name="result" select="replace($result, '&#x0183;', '&#x0042;')" />
	<xsl:variable name="result" select="replace($result, '&#x0186;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x0187;', '&#x0043;')" />
	<xsl:variable name="result" select="replace($result, '&#x0188;', '&#x0063;')" />
	<xsl:variable name="result" select="replace($result, '&#x0189;', '&#x0044;')" />
	<xsl:variable name="result" select="replace($result, '&#x018A;', '&#x0044;')" />
	<xsl:variable name="result" select="replace($result, '&#x018B;', '&#x0044;')" />
	<xsl:variable name="result" select="replace($result, '&#x018C;', '&#x0064;')" />
	<xsl:variable name="result" select="replace($result, '&#x018D;', '&#x0064;')" />
	<xsl:variable name="result" select="replace($result, '&#x018E;', '&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x0190;', '&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x0191;', '&#x0046;')" />
	<xsl:variable name="result" select="replace($result, '&#x0119;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x011A;', '&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x011B;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x011C;', '&#x0047;')" />
	<xsl:variable name="result" select="replace($result, '&#x011D;', '&#x0067;')" />
	<xsl:variable name="result" select="replace($result, '&#x011E;', '&#x0047;')" />
	<xsl:variable name="result" select="replace($result, '&#x011F;', '&#x0067;')" />
	<xsl:variable name="result" select="replace($result, '&#x0120;', '&#x0047;')" />
	<xsl:variable name="result" select="replace($result, '&#x0121;', '&#x0067;')" />
	<xsl:variable name="result" select="replace($result, '&#x0122;', '&#x0047;')" />
	<xsl:variable name="result" select="replace($result, '&#x0123;', '&#x0067;')" />
	<xsl:variable name="result" select="replace($result, '&#x0124;', '&#x0048;')" />
	<xsl:variable name="result" select="replace($result, '&#x0125;', '&#x0068;')" />
	<xsl:variable name="result" select="replace($result, '&#x0126;', '&#x0048;')" />
	<xsl:variable name="result" select="replace($result, '&#x0127;', '&#x0068;')" />
	<xsl:variable name="result" select="replace($result, '&#x0128;', '&#x0049;')" />
	<xsl:variable name="result" select="replace($result, '&#x0129;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x00FE;', '&#x0074;-&#x0068;')" />
	<xsl:variable name="result" select="replace($result, '&#x0132;', '&#x0069;-&#x006A;')" />
	<xsl:variable name="result" select="replace($result, '&#x0133;', '&#x0069;-&#x006A;')" />
	<xsl:variable name="result" select="replace($result, '&#x014A;', '&#x006E;')" />
	<xsl:variable name="result" select="replace($result, '&#x014B;', '&#x006E;')" />
	<xsl:variable name="result" select="replace($result, '&#x0152;', '&#x006F;-&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x0153;', '&#x006F;-&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x017F;', '&#x0073;')" />
	<xsl:variable name="result" select="replace($result, '&#x00BC;', '&#x0031;-&#x0034;')" />
	<xsl:variable name="result" select="replace($result, '&#x00BD;', '&#x0031;-&#x0032;')" />
	<xsl:variable name="result" select="replace($result, '&#x00BE;', '&#x0033;-&#x0034;')" />
	<xsl:variable name="result" select="replace($result, '&#x2070;', '&#x0030;')" />
	<xsl:variable name="result" select="replace($result, '&#x2071;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x2074;', '&#x0034;')" />
	<xsl:variable name="result" select="replace($result, '&#x2075;', '&#x0035;')" />
	<xsl:variable name="result" select="replace($result, '&#x2076;', '&#x0036;')" />
	<xsl:variable name="result" select="replace($result, '&#x2077;', '&#x0037;')" />
	<xsl:variable name="result" select="replace($result, '&#x2078;', '&#x0038;')" />
	<xsl:variable name="result" select="replace($result, '&#x2079;', '&#x0039;')" />
	<xsl:variable name="result" select="replace($result, '&#x207A;', '&#x002B;')" />
	<xsl:variable name="result" select="replace($result, '&#x207B;', '&#x002D;')" />
	<xsl:variable name="result" select="replace($result, '&#x207C;', '&#x003D;')" />
	<xsl:variable name="result" select="replace($result, '&#x207D;', '&#x0028;')" />
	<xsl:variable name="result" select="replace($result, '&#x207E;', '&#x0028;')" />
	<xsl:variable name="result" select="replace($result, '&#x207F;', '&#x004E;')" />
	<xsl:variable name="result" select="replace($result, '&#x2080;', '&#x0030;')" />
	<xsl:variable name="result" select="replace($result, '&#x2081;', '&#x0031;')" />
	<xsl:variable name="result" select="replace($result, '&#x2082;', '&#x0032;')" />
	<xsl:variable name="result" select="replace($result, '&#x2083;', '&#x0033;')" />
	<xsl:variable name="result" select="replace($result, '&#x2084;', '&#x0034;')" />
	<xsl:variable name="result" select="replace($result, '&#x2085;', '&#x0035;')" />
	<xsl:variable name="result" select="replace($result, '&#x2086;', '&#x0036;')" />
	<xsl:variable name="result" select="replace($result, '&#x2087;', '&#x0037;')" />
	<xsl:variable name="result" select="replace($result, '&#x2088;', '&#x0038;')" />
	<xsl:variable name="result" select="replace($result, '&#x2089;', '&#x0039;')" />
	<xsl:variable name="result" select="replace($result, '&#x208A;', '&#x002B;')" />
	<xsl:variable name="result" select="replace($result, '&#x208B;', '&#x002D;')" />
	<xsl:variable name="result" select="replace($result, '&#x208C;', '&#x003D;')" />
	<xsl:variable name="result" select="replace($result, '&#x208D;', '&#x0028;')" />
	<xsl:variable name="result" select="replace($result, '&#x208E;', '&#x0029;')" />
	<xsl:variable name="result" select="replace($result, '&#x208E;', '&#x0029;')" />
	<xsl:variable name="result" select="replace($result, '&#x2090;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x2091;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x2092;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x2093;', '&#x0078;')" />
	<xsl:variable name="result" select="replace($result, '&#x2094;', '&#x0065;')" />
	<!-- <xsl:variable name="result" select="replace($result, '&#x002E;', '')" /> -->
	<xsl:variable name="result" select="replace($result, '&#x0026;', '&#x0026;')" />
	<xsl:variable name="result" select="replace($result, '&#x00A3;', '&#x0020;')" />
	<xsl:variable name="result" select="replace($result, '&#x00A9;', '&#x0020;')" />
	<xsl:variable name="result" select="replace($result, '&#x00B0;', '&#x0020;')" />
	<xsl:variable name="result" select="replace($result, '&#x00C0;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x00C1;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x00C2;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x00C3;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x00C4;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x00C5;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x00C6;', '&#x0041;-&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x00C7;', '&#x0043;')" />
	<xsl:variable name="result" select="replace($result, '&#x00C8;', '&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x00C9;', '&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x00CA;', '&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x00CB;', '&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x00CC;', '&#x0049;')" />
	<xsl:variable name="result" select="replace($result, '&#x00CD;', '&#x0049;')" />
	<xsl:variable name="result" select="replace($result, '&#x00CE;', '&#x0049;')" />
	<xsl:variable name="result" select="replace($result, '&#x00CF;', '&#x0049;')" />
	<xsl:variable name="result" select="replace($result, '&#x00D0;', '&#x0044;')" />
	<xsl:variable name="result" select="replace($result, '&#x00D1;', '&#x004E;')" />
	<xsl:variable name="result" select="replace($result, '&#x00D2;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x00D3;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x00D4;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x00D5;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x00D6;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x00D7;', '&#x0020;')" />
	<xsl:variable name="result" select="replace($result, '&#x00D8;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x00D9;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x00DA;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x00DB;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x00DC;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x00DD;', '&#x0059;')" />
	<xsl:variable name="result" select="replace($result, '&#x00DE;', '&#x0054;-&#x0048;')" />
	<xsl:variable name="result" select="replace($result, '&#x00E0;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x00E1;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x00E2;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x00E3;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x00E4;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x00E5;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x00E6;', '&#x0061;-&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x00E7;', '&#x0063;')" />
	<xsl:variable name="result" select="replace($result, '&#x00E8;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x00E9;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x00EA;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x00EB;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x00EC;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x00ED;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x00EE;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x00EF;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x00F0;', '&#x0064;')" />
	<xsl:variable name="result" select="replace($result, '&#x00F1;', '&#x006E;')" />
	<xsl:variable name="result" select="replace($result, '&#x00F2;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x00F3;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x00F4;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x00F5;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x00F6;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x00F8;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x00F9;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x00FA;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x00FB;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x00FC;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x00FD;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x00FE;', '&#x0064;-&#x0068;')" />
	<xsl:variable name="result" select="replace($result, '&#x00FF;', '&#x0079;')" />
	<xsl:variable name="result" select="replace($result, '&#x0100;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x0101;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x0102;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x0103;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x0104;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x0105;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x0106;', '&#x0043;')" />
	<xsl:variable name="result" select="replace($result, '&#x0107;', '&#x0063;')" />
	<xsl:variable name="result" select="replace($result, '&#x0108;', '&#x0043;')" />
	<xsl:variable name="result" select="replace($result, '&#x0109;', '&#x0063;')" />
	<xsl:variable name="result" select="replace($result, '&#x010A;', '&#x0043;')" />
	<xsl:variable name="result" select="replace($result, '&#x010B;', '&#x0063;')" />
	<xsl:variable name="result" select="replace($result, '&#x010C;', '&#x0043;')" />
	<xsl:variable name="result" select="replace($result, '&#x0251;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x0252;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x0253;', '&#x0062;')" />
	<xsl:variable name="result" select="replace($result, '&#x0254;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x0255;', '&#x0063;')" />
	<xsl:variable name="result" select="replace($result, '&#x0256;', '&#x0064;')" />
	<xsl:variable name="result" select="replace($result, '&#x0257;', '&#x0064;')" />
	<xsl:variable name="result" select="replace($result, '&#x0258;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x025B;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x025C;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x025D;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x025E;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x025F;', '&#x006A;')" />
	<xsl:variable name="result" select="replace($result, '&#x0260;', '&#x0067;')" />
	<xsl:variable name="result" select="replace($result, '&#x0261;', '&#x0067;')" />
	<xsl:variable name="result" select="replace($result, '&#x0262;', '&#x0067;')" />
	<xsl:variable name="result" select="replace($result, '&#x0263;', '&#x0067;')" />
	<xsl:variable name="result" select="replace($result, '&#x0266;', '&#x0068;')" />
	<xsl:variable name="result" select="replace($result, '&#x0268;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x026A;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x026B;', '&#x006C;')" />
	<xsl:variable name="result" select="replace($result, '&#x026C;', '&#x006C;')" />
	<xsl:variable name="result" select="replace($result, '&#x026D;', '&#x006C;')" />
	<xsl:variable name="result" select="replace($result, '&#x0271;', '&#x006D;')" />
	<xsl:variable name="result" select="replace($result, '&#x0272;', '&#x006E;')" />
	<xsl:variable name="result" select="replace($result, '&#x0273;', '&#x006E;')" />
	<xsl:variable name="result" select="replace($result, '&#x0274;', '&#x006E;')" />
	<xsl:variable name="result" select="replace($result, '&#x0275;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x0276;', '&#x006F;-&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x0280;', '&#x0072;')" />
	<xsl:variable name="result" select="replace($result, '&#x0282;', '&#x0073;')" />
	<xsl:variable name="result" select="replace($result, '&#x0284;', '&#x006A;')" />
	<xsl:variable name="result" select="replace($result, '&#x0286;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0287;', '&#x0074;')" />
	<xsl:variable name="result" select="replace($result, '&#x0288;', '&#x0074;')" />
	<xsl:variable name="result" select="replace($result, '&#x0289;', '&#x0075;')" />
	<xsl:variable name="result" select="replace($result, '&#x028B;', '&#x0076;')" />
	<xsl:variable name="result" select="replace($result, '&#x028C;', '&#x0076;')" />
	<xsl:variable name="result" select="replace($result, '&#x028D;', '&#x0077;')" />
	<xsl:variable name="result" select="replace($result, '&#x028E;', '&#x0079;')" />
	<xsl:variable name="result" select="replace($result, '&#x028F;', '&#x0079;')" />
	<xsl:variable name="result" select="replace($result, '&#x0290;', '&#x007A;')" />
	<xsl:variable name="result" select="replace($result, '&#x0291;', '&#x007A;')" />
	<xsl:variable name="result" select="replace($result, '&#x0299;', '&#x0062;')" />
	<xsl:variable name="result" select="replace($result, '&#x029A;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x029B;', '&#x0067;')" />
	<xsl:variable name="result" select="replace($result, '&#x029C;', '&#x0068;')" />
	<xsl:variable name="result" select="replace($result, '&#x029D;', '&#x006A;')" />
	<xsl:variable name="result" select="replace($result, '&#x029E;', '&#x006B;')" />
	<xsl:variable name="result" select="replace($result, '&#x029F;', '&#x006C;')" />
	<xsl:variable name="result" select="replace($result, '&#x02A0;', '&#x0071;')" />
	<xsl:variable name="result" select="replace($result, '&#x02A3;', '&#x0064;-&#x007A;')" />
	<xsl:variable name="result" select="replace($result, '&#x02A5;', '&#x0064;-&#x007A;')" />
	<xsl:variable name="result" select="replace($result, '&#x02A6;', '&#x0074;-&#x0073;')" />
	<xsl:variable name="result" select="replace($result, '&#x02A8;', '&#x0074;-&#x0063;')" />
	<xsl:variable name="result" select="replace($result, '&#x02B0;', '&#x0068;')" />
	<xsl:variable name="result" select="replace($result, '&#x02B1;', '&#x0068;')" />
	<xsl:variable name="result" select="replace($result, '&#x02B2;', '&#x006A;')" />
	<xsl:variable name="result" select="replace($result, '&#x02B3;', '&#x0072;')" />
	<xsl:variable name="result" select="replace($result, '&#x02B4;', '&#x0072;')" />
	<xsl:variable name="result" select="replace($result, '&#x02B5;', '&#x0072;')" />
	<xsl:variable name="result" select="replace($result, '&#x02B6;', '&#x0072;')" />
	<xsl:variable name="result" select="replace($result, '&#x02B7;', '&#x0077;')" />
	<xsl:variable name="result" select="replace($result, '&#x02B8;', '&#x0079;')" />
	<xsl:variable name="result" select="replace($result, '&#x02B9;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02BA;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02BB;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02BC;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02BD;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02BE;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02BF;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02C0;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02C1;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02C2;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02C3;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02C4;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02C5;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02C6;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02C7;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02C8;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02C9;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02CA;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02CB;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02CC;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02CD;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02CE;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02CF;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02D0;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02D1;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x00C6;', '&#x0061;-&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x00DE;', '&#x0074;-&#x0068;')" />
	<xsl:variable name="result" select="replace($result, '&#x00DF;', '&#x0073;-&#x0073;')" />
	<xsl:variable name="result" select="replace($result, '&#x00E6;', '&#x0061;-&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x01AE;', '&#x0054;')" />
	<xsl:variable name="result" select="replace($result, '&#x01AF;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x01B0;', '&#x0075;')" />
	<xsl:variable name="result" select="replace($result, '&#x01B1;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x01B2;', '&#x0056;')" />
	<xsl:variable name="result" select="replace($result, '&#x01B3;', '&#x0059;')" />
	<xsl:variable name="result" select="replace($result, '&#x01B4;', '&#x0079;')" />
	<xsl:variable name="result" select="replace($result, '&#x01B5;', '&#x005A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01B6;', '&#x007A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01C4;', '&#x0044;-&#x005A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01C5;', '&#x0044;-&#x007A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01C6;', '&#x0064;-&#x007A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01C7;', '&#x004C;-&#x004A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01C8;', '&#x004C;-&#x006A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01C9;', '&#x006C;-&#x006A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01CA;', '&#x004E;-&#x004A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01CB;', '&#x004E;-&#x006A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01CC;', '&#x006E;-&#x006A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01CD;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x01CE;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x01CF;', '&#x0049;')" />
	<xsl:variable name="result" select="replace($result, '&#x01D0;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x01D1;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x01D2;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x01D3;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x01D4;', '&#x0075;')" />
	<xsl:variable name="result" select="replace($result, '&#x01D5;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x01D6;', '&#x0075;')" />
	<xsl:variable name="result" select="replace($result, '&#x01D7;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x01D8;', '&#x0075;')" />
	<xsl:variable name="result" select="replace($result, '&#x01D9;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x01DA;', '&#x0075;')" />
	<xsl:variable name="result" select="replace($result, '&#x01DB;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x01DC;', '&#x0075;')" />
	<xsl:variable name="result" select="replace($result, '&#x01DD;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x01DE;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x01DF;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x01E0;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x01E1;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x01E2;', '&#x0041;-&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x01E3;', '&#x0061;-&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x01E4;', '&#x0047;')" />
	<xsl:variable name="result" select="replace($result, '&#x01E5;', '&#x0067;')" />
	<xsl:variable name="result" select="replace($result, '&#x01E6;', '&#x0047;')" />
	<xsl:variable name="result" select="replace($result, '&#x01E7;', '&#x0067;')" />
	<xsl:variable name="result" select="replace($result, '&#x01E8;', '&#x004B;')" />
	<xsl:variable name="result" select="replace($result, '&#x01E9;', '&#x006B;')" />
	<xsl:variable name="result" select="replace($result, '&#x01EA;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x01EB;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x01EC;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x01ED;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x01F0;', '&#x006A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01F1;', '&#x0044;-&#x005A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01F2;', '&#x0044;-&#x007A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01F3;', '&#x0074;-&#x007A;')" />
	<xsl:variable name="result" select="replace($result, '&#x01F4;', '&#x0047;')" />
	<xsl:variable name="result" select="replace($result, '&#x01F5;', '&#x0067;')" />
	<xsl:variable name="result" select="replace($result, '&#x01FA;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x01FB;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x01FC;', '&#x0041;-&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x01FD;', '&#x0061;-&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x01FE;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x01FF;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x0200;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x0201;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x0202;', '&#x0041;')" />
	<xsl:variable name="result" select="replace($result, '&#x0203;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x0204;', '&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x0205;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x0206;', '&#x0045;')" />
	<xsl:variable name="result" select="replace($result, '&#x0207;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x0208;', '&#x0049;')" />
	<xsl:variable name="result" select="replace($result, '&#x0209;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x020A;', '&#x0049;')" />
	<xsl:variable name="result" select="replace($result, '&#x020B;', '&#x0069;')" />
	<xsl:variable name="result" select="replace($result, '&#x020C;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x020D;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x020E;', '&#x004F;')" />
	<xsl:variable name="result" select="replace($result, '&#x020F;', '&#x006F;')" />
	<xsl:variable name="result" select="replace($result, '&#x0210;', '&#x0052;')" />
	<xsl:variable name="result" select="replace($result, '&#x0211;', '&#x0062;')" />
	<xsl:variable name="result" select="replace($result, '&#x0212;', '&#x0052;')" />
	<xsl:variable name="result" select="replace($result, '&#x0213;', '&#x0062;')" />
	<xsl:variable name="result" select="replace($result, '&#x0214;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x0215;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x0216;', '&#x0055;')" />
	<xsl:variable name="result" select="replace($result, '&#x0217;', '&#x0065;')" />
	<xsl:variable name="result" select="replace($result, '&#x0250;', '&#x0061;')" />
	<xsl:variable name="result" select="replace($result, '&#x02D2;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02D3;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02D4;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02D5;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02D6;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02D7;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02D8;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02D9;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02DA;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02DB;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02DC;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02DD;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02DE;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02E0;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02E1;', '&#x004C;')" />
	<xsl:variable name="result" select="replace($result, '&#x02E2;', '&#x0053;')" />
	<xsl:variable name="result" select="replace($result, '&#x02E3;', '&#x0058;')" />
	<xsl:variable name="result" select="replace($result, '&#x02E4;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02E5;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02E6;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02E7;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02E8;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x02E9;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0300;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0301;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0302;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0303;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0304;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0305;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0306;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0307;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0308;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0309;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x030A;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x030B;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x030C;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x030D;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x030E;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x030F;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0310;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0311;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0312;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0313;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0314;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0315;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0316;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0317;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0318;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0319;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x031A;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x031B;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x031C;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x031D;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x031E;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x031F;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0320;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0321;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0322;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0323;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0324;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0325;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0326;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0327;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0328;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0329;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x032A;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x032B;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x032C;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x032D;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x032E;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x032F;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0330;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0331;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0332;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0333;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0334;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0335;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0336;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0337;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0338;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0339;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x033A;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x033B;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x033C;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x033D;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x033E;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x033F;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0340;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0341;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0360;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x0361;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x2070;', '&#x0030;')" />
	<xsl:variable name="result" select="replace($result, '&#x2071;', '&#x0031;')" />
	<xsl:variable name="result" select="replace($result, '&#x2072;', '&#x0032;')" />
	<xsl:variable name="result" select="replace($result, '&#x2073;', '&#x0033;')" />
	<xsl:variable name="result" select="replace($result, '&#x2074;', '&#x0034;')" />
	<xsl:variable name="result" select="replace($result, '&#x2075;', '&#x0035;')" />
	<xsl:variable name="result" select="replace($result, '&#x2076;', '&#x0036;')" />
	<xsl:variable name="result" select="replace($result, '&#x2077;', '&#x0037;')" />
	<xsl:variable name="result" select="replace($result, '&#x2078;', '&#x0038;')" />
	<xsl:variable name="result" select="replace($result, '&#x2079;', '&#x0039;')" />
	<xsl:variable name="result" select="replace($result, '&#x207A;', '&#x0020;')" />
	<xsl:variable name="result" select="replace($result, '&#x207B;', '&#x0020;')" />
	<xsl:variable name="result" select="replace($result, '&#x207C;', '&#x003D;')" />
	<xsl:variable name="result" select="replace($result, '&#x207D;', '&#x0020;')" />
	<xsl:variable name="result" select="replace($result, '&#x207E;', '&#x0020;')" />
	<xsl:variable name="result" select="replace($result, '&#x207F;', '&#x207F;')" />
	<xsl:variable name="result" select="replace($result, '&#x2080;', '&#x0030;')" />
	<xsl:variable name="result" select="replace($result, '&#x2081;', '&#x0031;')" />
	<xsl:variable name="result" select="replace($result, '&#x2082;', '&#x0032;')" />
	<xsl:variable name="result" select="replace($result, '&#x2083;', '&#x0033;')" />
	<xsl:variable name="result" select="replace($result, '&#x2084;', '&#x0034;')" />
	<xsl:variable name="result" select="replace($result, '&#x2085;', '&#x0035;')" />
	<xsl:variable name="result" select="replace($result, '&#x2086;', '&#x0036;')" />
	<xsl:variable name="result" select="replace($result, '&#x2087;', '&#x0037;')" />
	<xsl:variable name="result" select="replace($result, '&#x2088;', '&#x0038;')" />
	<xsl:variable name="result" select="replace($result, '&#x2089;', '&#x0039;')" />
	<xsl:variable name="result" select="replace($result, '&#x208A;', '&#x0020;')" />
	<xsl:variable name="result" select="replace($result, '&#x208B;', '&#x0020;')" />
	<xsl:variable name="result" select="replace($result, '&#x208C;', '&#x003D;')" />
	<xsl:variable name="result" select="replace($result, '&#x208D;', '&#x0020;')" />
	<xsl:variable name="result" select="replace($result, '&#x208E;', '&#x0020;')" />
	<xsl:variable name="result" select="replace($result, '&#x3000;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3001;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3002;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3003;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3004;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3005;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3006;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3007;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3008;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3009;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x300A;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x300B;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x300C;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x300D;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x300E;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x300F;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3010;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3011;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3012;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3013;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3014;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3015;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3016;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3017;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3018;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3019;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x301A;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x301B;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x301C;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x301D;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x301E;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x301F;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3020;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3021;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3022;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3023;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3024;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3025;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3026;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3027;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3028;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3029;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x302A;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x302B;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x302C;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x302D;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x302E;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x302F;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3030;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3031;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3032;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3033;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3034;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3035;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3036;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3037;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3038;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x3039;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x303A;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x303E;', '')" />
	<xsl:variable name="result" select="replace($result, '&#x303F;', '')" />
	<xsl:variable name="result" select="replace($result, '&#xFE20;', '')" />
	<xsl:variable name="result" select="replace($result, '&#xFE21;', '')" />
	<xsl:variable name="result" select="replace($result, '&#xFE22;', '')" />
	<xsl:variable name="result" select="replace($result, '&#xFE23;', '')" />

	<xsl:value-of select="$result" />
</xsl:function>


</xsl:stylesheet>
