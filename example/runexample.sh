#!/bin/bash

# Set exit on error
set -e

# Change to this directory
cd $( dirname "$0" )

# Verify that java is installed
echo "Checking that java is installed. JRE 1.6+ required"
java -version
echo ""

# Check if Saxon is available
echo "Checking that Saxon is available"
if [ ! -f saxon9he.jar ]; then
    echo "Please download Saxon-HE (saxon9he.jar) from http://saxon.sourceforge.net/ and place it in the same directory as this script"
    exit
fi

echo "Found saxon9he.jar"
echo ""

# Run the XSL through Saxon
echo "Transforming record.xml to pnx.xml using ../xsl/marc.xsl"
java -jar saxon9he.jar -xsl:../xsl/marc.xsl -s:record.xml -o:pnx.xml
echo "Done transforming record.xml. Please check pnx.xml"

